
import Foundation
import AVFoundation
import AVKit
import Combine

class GDAudioPlayer {
  
  public var trackEnd: ClousureEmpty!
  public var isPlay       = false
  public var isMute       = false
  public var trackPlaying = false
  
  static let shared = GDAudioPlayer()
  
  private var player      : AVPlayer!
  private var audioPlayer = AVAudioPlayer()
  
  //загрузка аудио файла
  public func load(fileName: FileNames, type: Types){
    guard StoreProject.shared.getBool(key: .isMusic) else { return }
    guard let file = Bundle.main.path(forResource: fileName.rawValue, ofType: type.rawValue) else { return }
    let url = URL(fileURLWithPath: file)
    do {
      self.audioPlayer = try AVAudioPlayer(contentsOf: url)
      self.audioPlayer.prepareToPlay()
      self.audioPlayer.numberOfLoops = -1
      self.audioPlayer.volume = 0.5
    } catch let error {
      print(error.localizedDescription)
    }
   
  }
  public func mute(){
    
    self.isMute.toggle()
  }
  public func pause(delay: Double = 0){
    DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [weak self] in
      guard let self = self else { return }
      self.audioPlayer.pause()
      self.isPlay = false
    }
  }
  public func play(){
    self.audioPlayer.play()
    self.isPlay = true
  }
  private func addObserver(){
    let nsTime = CMTime(seconds: 1, preferredTimescale: 1000)
    self.player.addPeriodicTimeObserver(forInterval: nsTime, queue: DispatchQueue.main) { (time) in
      let second   = self.player.currentTime().seconds
      let duration = self.player.currentItem?.duration.seconds ?? 0
      guard second == duration, self.isPlay else { return }
      self.isPlay = false
      guard self.trackPlaying else { return }
      self.isPlay = true
      self.trackEnd?()
    }
  }
  enum FileNames: String, CaseIterable {
    case background1
    case background2
    case roulette
    case winRoulette
  }
  enum Types: String, CaseIterable {
    
    case mp3
    case wav
  }
}
