
import Foundation

struct GDSettings {
  
  let openWB        : Bool!
  let path          : String!
  let advertisingEnd: Bool!
  
  enum CodingKeys: String, CodingKey {
    
    case openWB = "open_wb"
    case path   = "path"
    case advertisingEnd
  }
}
extension GDSettings: Decodable {
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    
    self.openWB          = try? values.decode(Bool?.self  , forKey: .openWB)
    self.path            = try? values.decode(String?.self, forKey: .path)
    self.advertisingEnd  = try? values.decode(Bool?.self  , forKey: .advertisingEnd)
  }
}
