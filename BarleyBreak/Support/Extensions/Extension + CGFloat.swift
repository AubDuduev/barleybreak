
import Foundation

import UIKit
//1° × π/180 = 0,01745 рад
extension CGFloat {
  static func radians(gr: CGFloat) -> CGFloat {
    let result = gr * (CGFloat.pi / 180)
    return result
  }
}
