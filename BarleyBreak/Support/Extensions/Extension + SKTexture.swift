
import SpriteKit

extension SKTexture {
  
  convenience init(texture: TextureName) {
    self.init(imageNamed: texture.rawValue)
  }
  
  static func name(_ texture: TextureName) -> SKTexture {
    let texture = SKTexture(imageNamed: texture.rawValue)
  return texture
  }
  static func textures(_ textures: [String]) -> [SKTexture] {
    
    var returnTextures = [SKTexture]()
    for imageNamed in textures {
      let texture = SKTexture(imageNamed: imageNamed)
      returnTextures.append(texture)
    }
  return returnTextures
  }
}
