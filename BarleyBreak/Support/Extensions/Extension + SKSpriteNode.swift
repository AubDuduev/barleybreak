
import SceneKit
import GameplayKit

extension SKSpriteNode {
  
  static func name(_ name: NodeName) -> SKSpriteNode {
    let node = SKSpriteNode(imageNamed: name.rawValue)
  return node
  }
  static func name(_ name: String) -> SKSpriteNode {
    let node = SKSpriteNode(imageNamed: name)
  return node
  }
  static func name(_ texture: TextureName) -> SKSpriteNode {
    let texture = SKTexture(imageNamed: texture.rawValue)
    let node = SKSpriteNode(texture: texture)
  return node
  }
  
  convenience init(name: NodeName) {
    self.init(imageNamed: name.rawValue)
  }
  convenience init(texture: TextureName) {
    let texture = SKTexture(imageNamed: texture.rawValue)
    self.init(texture: texture)
  }
  convenience init(name: String) {
    self.init(imageNamed: name)
  }
  
  func animationTexture(textures: [SKTexture], time: Double, type animation: AnimationTextureType, resize: Bool = false, restore: Bool = false, completion: ClousureEmpty? = nil){
    
    
    let action =  SKAction.animate(with        : textures,
                                   timePerFrame: time,
                                   resize      : resize,
                                   restore     : restore)
    
    
    switch animation {
      
      case .Repeat:
        let actionsRepeat = SKAction.repeatForever(action)
        self.run(actionsRepeat)
      case .One:
        self.run(action){
          completion?()
        }
      case .OneRemove:
        self.run(action) {
          self.removeFromParent()
        }
    }
  }
  
  enum AnimationTextureType {
    case Repeat
    case OneRemove
    case One
  }
  
  func sound(file: SoundFile) {
    if StoreProject.shared.getBool(key: .isSound) {
      let soundAction = SKAction.playSoundFileNamed(file.rawValue, waitForCompletion: false)
      self.run(soundAction)
    } else {
      let soundAction = SKAction.wait(forDuration: 0)
      self.run(soundAction)
    }
  }
  public func move(dx: CGFloat = 0, dy: CGFloat = 0, duration: TimeInterval){
    let moveForward = SKAction.move(by: CGVector(dx: dx, dy: dy), duration: duration)
    self.run(moveForward, withKey: "moveBackgraund")
  }
}




