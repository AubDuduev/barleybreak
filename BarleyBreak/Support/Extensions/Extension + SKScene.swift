
import SpriteKit
import Foundation

extension SKScene {
  
  
  convenience init?(fileNamed: SceneName) {
    self.init(fileNamed: fileNamed.rawValue)
  }
}
