
import Foundation

extension Notification.Name {
	
	static func id(_ id: Notification.Name.ID) -> Notification.Name {
		let name = Notification.Name(id.rawValue)
		return name
	}
	
	enum ID: String {
		case getURL
		case notificationSusses
	}
}

