
import WebKit

extension WKWebView {
   
   func set(string: String? = nil, urls: URLString.Url? = nil, urlHTML: URLString.HTML? = nil){
      
      guard let url = self.create(string: string, urls: urls, urlHTML: urlHTML) else { return }
      // Cookies configuration
      var urlRequest = URLRequest(url: url)
      if let cookies = HTTPCookieStorage.shared.cookies(for: url) {
         let headers = HTTPCookie.requestHeaderFields(with: cookies)
         headers.forEach({ urlRequest.addValue($0.value, forHTTPHeaderField: $0.key) })
         self.load(urlRequest)
      } else {
         self.load(urlRequest)
      }
      
   }
   private func create(string: String? = nil, urls: URLString.Url? = nil, urlHTML: URLString.HTML? = nil) -> URL? {
      
      switch true {
         case string != nil:
            return URL(string: string!)
         case urls    != nil:
            return URL(string: urls!.rawValue)
         case urlHTML != nil:
            return Bundle.main.url(forResource: urlHTML!.rawValue, withExtension: "html")
         default:
            return nil
      }
   }
}


//// Cookies configuration
//var urlRequest = URLRequest(url: url)
//if let cookies = HTTPCookieStorage.shared.cookies(for: url) {
//   cookies.forEach({
//      HTTPCookieStorage.shared.setCookie($0)
//   })
//   let headers = HTTPCookie.requestHeaderFields(with: cookies)
//   headers.forEach({ urlRequest.addValue($0.value, forHTTPHeaderField: $0.key) })
//}
//let dataTask = URLSession.shared.dataTask(with: url)
//
//HTTPCookieStorage.shared.getCookiesFor(dataTask) { cookies in
//   
//   let headers = HTTPCookie.requestHeaderFields(with: cookies!)
//   headers.forEach({ urlRequest.addValue($0.value, forHTTPHeaderField: $0.key) })
//   self.load(urlRequest)
//}
