
import SpriteKit
import Foundation

extension SKView {
  
  func scene(name: SceneName){
    let scene = SKScene(fileNamed: name.rawValue)
    scene?.scaleMode = .aspectFill
    self.presentScene(scene)
  }
  enum SceneName: String, CaseIterable {
    case GameSC = "GameSceneController"
  }
}
