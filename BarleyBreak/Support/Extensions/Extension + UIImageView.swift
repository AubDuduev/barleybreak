//
//  Extension + UIImageView.swift
//  Inters
//
//  Created by DEVELOPER on 19/03/2020.
//  Copyright © 2020 DEVELOPER. All rights reserved.
//

import UIKit

extension UIImageView {
	
	func set(nameImage: NameImage){
		self.image = UIImage(named: nameImage.rawValue)
	}
	
	enum NameImage: String {
		
    case batman
    case chinese
    case deadpool
    case lego
    case legoGirl
    case listening
    case mariachi
    case medicalMask
    case spiderman
    case superman
	}
}



