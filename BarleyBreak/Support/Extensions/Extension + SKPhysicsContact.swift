
import SpriteKit

extension SKPhysicsContact {
  
  func contact(bodyA: BitMasks.Types, bodyB: BitMasks.Types) -> Bool {
   
    //1 - Первый обект равен первой выбраной маске
    if self.bodyB.equalContactTest(bodyB) {
      //2 - Второй обект равен второй выбраной маске
      if self.bodyA.equalContactTest(bodyA) {
        return true
      }
    }
    //1 - Первый обект равен второй выбраной маске
    if self.bodyB.equalContactTest(bodyA) {
      //2 - Второй обект равен первой выбраной маске
      if self.bodyA.equalContactTest(bodyB) {
        return true
      }
    }
    return false
  }
  func colision(bodyA: BitMasks.Types, bodyB: BitMasks.Types) -> Bool {
    //1 - Первый обект равен первой выбраной маске
    if self.bodyB.equalCollision(bodyB) {
      //2 - Второй обект равен второй выбраной маске
      if self.bodyA.equalCollision(bodyA) {
        return true
      }
    }
    //1 - Первый обект равен второй выбраной маске
    if self.bodyB.equalCollision(bodyA) {
      //2 - Второй обект равен первой выбраной маске
      if self.bodyA.equalCollision(bodyB) {
        return true
      }
    }
    return false
  }
  func category(bodyA: BitMasks.Types, bodyB: BitMasks.Types) -> Bool {
    //1 - Первый обект равен первой выбраной маске
    if self.bodyB.equalCategory(bodyB) {
      //2 - Второй обект равен второй выбраной маске
      if self.bodyA.equalCategory(bodyA) {
        return true
      }
    }
    //1 - Первый обект равен второй выбраной маске
    if self.bodyB.equalCategory(bodyA) {
      //2 - Второй обект равен первой выбраной маске
      if self.bodyA.equalCategory(bodyB) {
        return true
      }
    }
    return false
  }
}
