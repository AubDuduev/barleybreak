
import SpriteKit

extension SKPhysicsBody {
  
  
  func equalContactTest(_ mask: BitMasks.Types) -> Bool {
    return self.contactTestBitMask == mask.rawValue
  }
  func equalCollision(_ mask: BitMasks.Types) -> Bool {
    return self.collisionBitMask == mask.rawValue
  }
  func equalCategory(_ mask: BitMasks.Types) -> Bool {
    return self.categoryBitMask == mask.rawValue
  }
}
