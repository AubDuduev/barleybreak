
import Foundation

enum CardsOne: String, CaseIterable {
  
  case cardOne1
  case cardOne2
  case cardOne3
  case cardOne4
  case cardOne5
  case cardOne6
  case cardOne7
  case cardOne8
  case cardOne9
  case cardOne10
  case cardOne11
  case cardOne12
  case cardOne13
  case cardOne14
  case cardOne15
  
  static func string() -> [String]{
    return Self.allCases.map{ $0.rawValue }
  }
  static func shuffled() -> [String]{
    let array = Self.allCases.map{ $0.rawValue }
    return array.shuffled()
  }
  static func shuffled(count: Int) -> [String]{
    var items = [String]()
    let count = count * count
    for item in 0..<count {
      items.append(Self.allCases[item].rawValue)
    }
    return items.shuffled()
  }
  static func array(count: Int) -> [String]{
    var items = [String]()
    let count = count * count
    for item in 0..<(count - 1) {
      items.append(Self.allCases[item].rawValue)
    }
    return items
  }
  static func slots() -> [Self]{
    return Self.allCases.map{ $0}
  }
}

enum CardsTwo: String, CaseIterable {
  
  case cardTwo1
  case cardTwo2
  case cardTwo3
  case cardTwo4
  case cardTwo5
  case cardTwo6
  case cardTwo7
  case cardTwo8
  case cardTwo9
  case cardTwo10
  case cardTwo11
  case cardTwo12
  case cardTwo13
  case cardTwo14
  case cardTwo15
  
  static func string() -> [String]{
    return Self.allCases.map{ $0.rawValue }
  }
  static func shuffled() -> [String]{
    let array = Self.allCases.map{ $0.rawValue }
    return array.shuffled()
  }
  static func shuffled(count: Int) -> [String]{
    var items = [String]()
    let count = count * count
    for item in 0..<count {
      items.append(Self.allCases[item].rawValue)
    }
    return items.shuffled()
  }
  static func array(count: Int) -> [String]{
    var items = [String]()
    let count = count * count
    for item in 0..<(count - 1) {
      items.append(Self.allCases[item].rawValue)
    }
    return items
  }
  static func slots() -> [Self]{
    return Self.allCases.map{ $0}
  }
}

enum CardsThree: String, CaseIterable {
  
  case cardThree1
  case cardThree2
  case cardThree3
  case cardThree4
  case cardThree5
  case cardThree6
  case cardThree7
  case cardThree8
  case cardThree9
  case cardThree10
  case cardThree11
  case cardThree12
  case cardThree13
  case cardThree14
  case cardThree15
  
  static func string() -> [String]{
    return Self.allCases.map{ $0.rawValue }
  }
  static func shuffled() -> [String]{
    let array = Self.allCases.map{ $0.rawValue }
    return array.shuffled()
  }
  static func shuffled(count: Int) -> [String]{
    var items = [String]()
    let count = count * count
    for item in 0..<count {
      items.append(Self.allCases[item].rawValue)
    }
    return items.shuffled()
  }
  static func array(count: Int) -> [String]{
    var items = [String]()
    let count = count * count
    for item in 0..<(count - 1) {
      items.append(Self.allCases[item].rawValue)
    }
    return items
  }
  static func slots() -> [Self]{
    return Self.allCases.map{ $0}
  }
}

enum Cards: String, CaseIterable {
  
  case card1
  case card2
  case card3
  case card4
  case card5
  case card6
  case card7
  case card8
  case card9
  case card10
  case card11
  case card12
  case card13
  case card14
  case card15
  
  static func string() -> [String]{
    return Self.allCases.map{ $0.rawValue }
  }
  static func shuffled() -> [String]{
    let array = Self.allCases.map{ $0.rawValue }
    return array.shuffled()
  }
  static func shuffled(count: Int) -> [String]{
    var items = [String]()
    let count = count * count
    for item in 0..<count {
      items.append(Self.allCases[item].rawValue)
    }
    return items.shuffled()
  }
  static func array(count: Int) -> [String]{
    var items = [String]()
    let count = count * count
    for item in 0..<(count - 1) {
      items.append(Self.allCases[item].rawValue)
    }
    return items
  }
  static func slots() -> [Self]{
    return Self.allCases.map{ $0}
  }
}

enum CardsFore: String, CaseIterable {
  
  case cardFore1
  case cardFore2
  case cardFore3
  case cardFore4
  case cardFore5
  case cardFore6
  case cardFore7
  case cardFore8
  case cardFore9
  case cardFore10
  case cardFore11
  case cardFore12
  case cardFore13
  case cardFore14
  case cardFore15
  
  static func string() -> [String]{
    return Self.allCases.map{ $0.rawValue }
  }
  static func shuffled() -> [String]{
    let array = Self.allCases.map{ $0.rawValue }
    return array.shuffled()
  }
  static func shuffled(count: Int) -> [String]{
    var items = [String]()
    let count = count * count
    for item in 0..<count {
      items.append(Self.allCases[item].rawValue)
    }
    return items.shuffled()
  }
  static func array(count: Int) -> [String]{
    var items = [String]()
    let count = count * count
    for item in 0..<(count - 1) {
      items.append(Self.allCases[item].rawValue)
    }
    return items
  }
  static func slots() -> [Self]{
    return Self.allCases.map{ $0}
  }
}

