import Foundation

enum Backgraund: String, CaseIterable {
  
  case background
  
  static func get(name: Self) -> String {
    return name.rawValue
  }
}
