
import Foundation

enum Pigs: String, CaseIterable {
  
  case icons1
  case icons2
  case icons3
  case icons4
  case icons5
  case icons6
  case icons7
  case icons8
  case icons9
  case icons10
  
}
