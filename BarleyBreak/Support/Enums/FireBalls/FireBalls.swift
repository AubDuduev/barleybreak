//
//  FireBalls.swift
//  Fight
//
//  Created by Senior Developer on 25.11.2020.
//
import Foundation

enum FireBallsStart: String, CaseIterable {
  
  case FireBall1
  case FireBall2
  case FireBall3
  case FireBall4
  case FireBall5
  
  static func sprits() -> [String]{
    return Self.allCases.map{ $0.rawValue }
  }
}

enum FireBallsEnd: String, CaseIterable {
  
  case FireBall5
  case FireBall6
  case FireBall7
  case FireBall8
  
  static func sprits() -> [String]{
    return Self.allCases.map{ $0.rawValue }
  }
}
enum FireBallsBoom: String, CaseIterable {
  
  case FireBall1
  case FireBall2
  case FireBall3
  case FireBall4
  case FireBall5
  case FireBall6
  case FireBall7
  case FireBall8
  
  static func sprits() -> [String]{
    return Self.allCases.map{ $0.rawValue }
  }
}
