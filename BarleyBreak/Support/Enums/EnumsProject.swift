
import UIKit


enum SceneName: String, CaseIterable {
  case GameScene = "GameScene"
}

enum SoundFile: String {
  
  case boom       = "boom.mp3"
  case error      = "Error.wav"
  case spinSlots  = "spinSlots.mp3"
  case background1 = "background1.mp3"
  case background2 = "background2.mp3"
  case endGame    = "end.mp3"
  case jeckpot    = "jeckpot.mp3"
  case backCard   = "backcard.mp3"
  case roulette   = "roulette.mp3"
  case winRulete  = "winRulete.mp3"
  case endShort   = "endShort.mp3"
  case bonus      = "bonus.mp3"
  case spin       = "startAnimation.mp3"
  case compare    = "compareLine.mp3"
  case short      = "short.mp3"
}

enum Card: Int, CaseIterable {
  
  case valet
  case dama
  case korol
  case tuz
  case jocker

 
  public func image() -> UIImage? {
  
    switch self {
      case .tuz:
        return UIImage(named: "tuz")
      case .korol:
        return UIImage(named: "korol")
      case .dama:
        return UIImage(named: "dama")
      case .valet:
        return UIImage(named: "valet")
      case .jocker:
        return UIImage(named: "jocker")
    }
  }
}

enum CardGameState {
  
  case Win
  case Lose
  case Evil
  case Game
}

enum BonusDays: String, CaseIterable {
  
  case bonus1 = "100"
  case bonus2 = "200"
  case bonus3 = "300"
  case bonus4 = "400"
  case bonus5 = "500"
  case bonus6 = "600"
  case bonus7 = "700"
  case bonus8 = "800"
  case bonus9 = "900"
  case bonus10 = "1000"
  case bonus11 = "10000"
}
