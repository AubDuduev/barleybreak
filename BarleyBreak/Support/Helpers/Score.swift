
import Foundation
import UIKit

class Score {
  
  static let shared  = Score()
  private var balance = StoreProject.shared.getInt(key: .currentScore) ?? 3000
  private var balanceLabel: UILabel!
  
  public func setBalance(label: UILabel){
    self.balance = StoreProject.shared.getInt(key: .currentScore) ?? 3000
    self.balanceLabel = label
    self.balanceLabel.text = String(balance)
    StoreProject.shared.saveInt(key: .currentScore, value: self.balance)
  }
  public func isBalance(sum: Int = 0) -> Bool {
    if sum == 0 {
      self.balance = StoreProject.shared.getInt(key: .currentScore) ?? 0
      return self.balance > 10
    } else {
      self.balance = StoreProject.shared.getInt(key: .currentScore) ?? 0
      self.balance -= sum
      return self.balance > 0
    }
  }
  public func calculate(type: CalculateType){
    self.balance = StoreProject.shared.getInt(key: .currentScore) ?? 3000
    switch type {
      case .plus(let coin):
        self.balance += coin
      case .minus(let coin):
        self.balance -= coin
    }
    StoreProject.shared.saveInt(key: .currentScore, value: self.balance)
  }
  
  enum CalculateType {
    
    case minus(Int)
    case plus(Int)
  }
}
