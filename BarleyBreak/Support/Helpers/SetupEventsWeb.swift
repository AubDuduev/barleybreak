//
//  SetupEventsWeb.swift
import Foundation

class SetupEventsWeb {

  public static let shared = SetupEventsWeb()
  
  public func dismiss(url: URL?, webVC: WebViewViewController){
    guard let stringURL = url?.absoluteString else { return }
    if stringURL.contains("advertising.end") {
      webVC.pushVCForID(storyboardID: .StartGame , vcID: .StartGameVC, animation: true)
    }
  }
  public func testURL(url: URL?){
    guard let stringURL = url?.absoluteString else { return }
    print(stringURL, "stringURL")
  }
}



