import CryptoKit
import UIKit
import AdSupport
import AppTrackingTransparency
import DeviceCheck

class IDFA {
  // MARK: - Stored Type Properties
  static let shared = IDFA()
  
  private var isTrackingEnabled: Bool {
    if "00000000-0000-0000-0000-000000000000" ==  ASIdentifierManager.shared().advertisingIdentifier.uuidString {
      return false
    } else {
      return true
    }
  }
  
  var identifier: String {
    if isTrackingEnabled {
      return ASIdentifierManager.shared().advertisingIdentifier.uuidString
    } else {
      return self.customIdentifier
    }
  }
 private var customIdentifier: String {
    let one1    = String(Int.random(in: 1000...9999))
    let one2    = String(Int.random(in: 1000...9999))
    let two1    = String(Int.random(in: 10...99))
    let two2    = String(Int.random(in: 10...99))
    let three1  = String(Int.random(in: 10...99))
    let three2  = String(Int.random(in: 10...99))
    let fore1   = String(Int.random(in: 10...99))
    let fore2   = String(Int.random(in: 10...99))
    let five1   = String(Int.random(in: 1000...9999))
    let five2   = String(Int.random(in: 1000...9999))
    let five3   = String(Int.random(in: 1000...9999))
    let result  = "\(one1)\(one2)-\(two1)\(two2)-\(three1)\(three2)-\(fore1)\(fore2)-\(five1)\(five2)\(five3)"
                 //"5B8B 46E4-8612-4667-9BF1-A869 8FC6 778D"
    return result//"0000 0000-0000-0000-0000-0000 0000 0000"
  }
  
  var vendor: String? {
    return UIDevice.current.identifierForVendor?.uuidString
  }

  public func getRequest(completion: @escaping Clousure<Bool>){
    if #available(iOS 14, *) {
      ATTrackingManager.requestTrackingAuthorization { (status) in
        switch status {
          case .notDetermined:
            completion(false)
          case .restricted:
            completion(false)
          case .denied:
            completion(false)
          case .authorized:
            completion(true)
          @unknown default:
            completion(false)
        }
      }
    //This request no accept IOS Version
    } else {
      completion(true)
    }
  }
}
