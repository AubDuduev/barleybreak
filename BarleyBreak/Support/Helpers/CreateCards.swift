
import SceneKit
import SpriteKit
import Foundation

class CreateCards {
  
  private var gameFon   : GameFonNode!
  private var gameScene : GameScene!
  
  public var cardNodes    = [CardNode]()
  public var matchesArray = [String]()
  public let arrayCards   = ArrayCards()
  
  public func create(count: Int){
    //Число которое является количеством ячеек
    let countCell = count
    //Картинки бриллиантов
    var imageNames    = arrayCards.array(count: count)
    self.matchesArray = imageNames
    //Название картинок для сравнения
    matchesArray.append("emptyCard")
    //Картинки для создания
    imageNames = imageNames.shuffled()
    imageNames.append("emptyCard")
    //Размер брилианта
    let elementSquare = self.gameFon.size.height / CGFloat(countCell)
    //Расположение первого бриллианта
    let transformPositionY = (self.gameFon.position.y + (self.gameFon.size.height / 2) - (elementSquare / 2))
    let transformPositionX = (self.gameFon.position.x - (self.gameFon.size.width  / 2) + (elementSquare / 2))
    var transformPosition  = CGPoint(x: transformPositionX, y: transformPositionY)
    //Количество бриллиантов
    let elementsCounts = Array.init(repeating: Int(), count: (countCell * countCell))
    var column = 0
    var row    = 0
    //Создаем и располагаем бриллианты
    for (index, _) in elementsCounts.enumerated() {
      column = (index % countCell)
      
      let imageNamed = imageNames[index]
      let cardNode   = CardNode(imageNamed    : imageNamed,
                                 position     : transformPosition,
                                 elementSquare: elementSquare)
      
      cardNode.setup(column: column, row: row, index: index, imageNamed: imageNamed, scene: self.gameScene)
      cardNodes.append(cardNode)
      print(transformPosition)
      
      if column == (countCell - 1) {
        transformPosition.x = transformPositionX
        transformPosition.y -= elementSquare
        row += 1
      } else {
        transformPosition.x += elementSquare
      }
     
    }
    
//    let emptyCardNode = cardNodes.removeLast()
//    emptyCardNode.texture = SKTexture(imageNamed: "emptyCard")
//    emptyCardNode.imageNamed = "emptyCard"
//    cardNodes.append(emptyCardNode)
    cardNodes.forEach{self.gameFon.addChild($0)}
  }
  init(gameFon: GameFonNode, gameScene: GameScene) {
    self.gameFon   = gameFon
    self.gameScene = gameScene
  }
}
