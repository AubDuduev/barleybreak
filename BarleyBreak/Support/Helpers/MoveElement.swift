//
//  MoveDiamond.swift

import CoreGraphics
import Foundation
import SpriteKit

class MoveElement {
  
  private let gameFon   : GameFonNode!
  private let emptyCard = "emptyCard"
  
  public func left(cardTouch: CardNode){
    let positionX = (cardTouch.position.x - cardTouch.size.width)
    let positionY = cardTouch.position.y
    let searchPosition = CGPoint(x: positionX, y: positionY)
    if let rightElement = gameFon.atPoint(searchPosition) as? CardNode, rightElement.imageNamed == emptyCard  {
      let leftElement = cardTouch
      rightElement.move(dx: cardTouch.size.width, dy: 0, duration: 0.3)
      leftElement.move(dx: -cardTouch.size.width, dy: 0, duration: 0.3)
      rightElement.run(SKAction.sound(file: .short))
    }
  }
  public func right(cardTouch: CardNode){
    
    let positionX = (cardTouch.position.x + cardTouch.size.width)
    let positionY = cardTouch.position.y
    let searchPosition = CGPoint(x: positionX, y: positionY)
    if let rightElement = gameFon.atPoint(searchPosition) as? CardNode, rightElement.imageNamed == emptyCard {
      let leftElement = cardTouch
      rightElement.move(dx: -cardTouch.size.width, dy: 0, duration: 0.3)
      leftElement.move(dx: cardTouch.size.width, dy: 0, duration: 0.3)
      rightElement.run(SKAction.sound(file: .short))
    }
  }
  public func up(cardTouch: CardNode){
    let positionX = cardTouch.position.x
    let positionY = (cardTouch.position.y + cardTouch.size.width)
    let searchPosition = CGPoint(x: positionX, y: positionY)
    if let topElement = gameFon.atPoint(searchPosition) as? CardNode, topElement.imageNamed == emptyCard  {
      let bottomElement = cardTouch
      bottomElement.move(dx: 0, dy: cardTouch.size.width, duration: 0.3)
      topElement.move(dx: 0, dy: -cardTouch.size.width, duration: 0.3)
      bottomElement.run(SKAction.sound(file: .short))
    }
  }
  public func down(cardTouch: CardNode){
    let positionX = cardTouch.position.x
    let positionY = (cardTouch.position.y - cardTouch.size.height)
    let searchPosition = CGPoint(x: positionX, y: positionY)
    if let topElement = gameFon.atPoint(searchPosition) as? CardNode, topElement.imageNamed == emptyCard  {
      let bottomElement = cardTouch
      bottomElement.move(dx: 0, dy: -cardTouch.size.width, duration: 0.3)
      topElement.move(dx: 0, dy: cardTouch.size.width, duration: 0.3)
      bottomElement.run(SKAction.sound(file: .short))
    }
  }
  init(gameFon: GameFonNode) {
    self.gameFon = gameFon
  }
}
