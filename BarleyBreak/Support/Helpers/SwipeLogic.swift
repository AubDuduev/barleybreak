//
//  SwipeLogic.swift
import SceneKit
import Foundation

class SwipeLogic {
  
  private let gameFon      : GameFonNode!
  private let gameScene    : GameScene!
  private let moveElement  : MoveElement!
  
  public var cardTouch: CardNode!
  
  public func setup(){
    let swipeLeft  = #selector(moveLeft)
    let swipeRight = #selector(moveRight)
    let swipeUp    = #selector(moveUp)
    let swipeDown  = #selector(moveDown)
    let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: swipeLeft)
    swipeLeftGesture.direction = .left
    let swipeUpGesture = UISwipeGestureRecognizer(target: self, action: swipeUp)
    swipeUpGesture.direction = .up
    let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: swipeRight)
    swipeRightGesture.direction = .right
    let swipeDownGesture = UISwipeGestureRecognizer(target: self, action: swipeDown)
    swipeDownGesture.direction = .down
    self.gameScene.view?.addGestureRecognizer(swipeLeftGesture)
    self.gameScene.view?.addGestureRecognizer(swipeUpGesture)
    self.gameScene.view?.addGestureRecognizer(swipeRightGesture)
    self.gameScene.view?.addGestureRecognizer(swipeDownGesture)
    self.gameScene.view?.isUserInteractionEnabled = true
  }
  @objc
  private func moveLeft(){
    guard self.cardTouch != nil else { return }
    print(#function)
    self.moveElement.left(cardTouch: self.cardTouch)
  }
  @objc
  private func moveRight(){
    guard self.cardTouch != nil else { return }
    print(#function)
    self.moveElement.right(cardTouch: self.cardTouch)
  }
  @objc
  private func moveUp(){
    guard self.cardTouch != nil else { return }
    print(#function)
    self.moveElement.up(cardTouch: self.cardTouch)
  }
  @objc
  private func moveDown(){
    guard self.cardTouch != nil else { return }
    print(#function)
    self.moveElement.down(cardTouch: self.cardTouch)
  }
  
  init(gameFon: GameFonNode, gameScene: GameScene, moveElement: MoveElement) {
    self.gameFon     = gameFon
    self.gameScene   = gameScene
    self.moveElement = moveElement
  }
}
