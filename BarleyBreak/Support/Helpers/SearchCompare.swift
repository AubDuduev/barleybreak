//
//  SearchCompare.swift

import SpriteKit

class SearchCompare {
  
  private let gameScene: GameScene!
  private let gameFon  : GameFonNode!
  
  public func searchAllCompare(count: Int){
    //Число которое является количеством ячеек
    let countCell = count
    //Размер брилианта
    let elementSquare = self.gameFon.size.height / CGFloat(countCell)
    //Расположение первого бриллианта
    let transformPositionY = (self.gameFon.position.y + (self.gameFon.size.height / 2) - (elementSquare / 2))
    let transformPositionX = (self.gameFon.position.x - (self.gameFon.size.width / 2)  + (elementSquare / 2))
    var transformPosition = CGPoint(x: transformPositionX, y: transformPositionY)
    //Количество бриллиантов
    let elementsCounts = Array.init(repeating: Int(), count: (countCell * countCell))
    var column = 0
    var row    = 0
    //Создаем и располагаем бриллианта
    for (index, _) in elementsCounts.enumerated() {
      column = (index % countCell)
      
      //Получаем по координате бриллиант
      if let element = self.gameFon.atPoint(transformPosition) as? CardNode {
      
        //сравниваем его с соседями
        self.compare(moveElement: element)
      }
      
      if column == (countCell - 1) {
        transformPosition.x = transformPositionX
        transformPosition.y -= elementSquare
        row += 1
      } else {
        transformPosition.x += elementSquare
      }
     
    }
  }
  public func compare(moveElement: CardNode){
    self.right(moveElement: moveElement)
    self.left(moveElement: moveElement)
    self.down(moveElement: moveElement)
    self.up(moveElement: moveElement)
  }
  private func right(moveElement: CardNode){
    let positionX = (moveElement.position.x + moveElement.size.width)
    let positionY =  moveElement.position.y
    let searchPosition = CGPoint(x: positionX, y: positionY)
    if let rightElement = gameFon.atPoint(searchPosition) as? CardNode {
      if rightElement.imageNamed == moveElement.imageNamed {
        
        let textures = SKTexture.textures(FireBallsBoom.sprits())
        rightElement.animationTexture(textures: textures, time: 0.1, type: .OneRemove)
        moveElement .animationTexture(textures: textures, time: 0.1, type: .OneRemove)
        moveElement.run(SKAction.sound(file: .boom))
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
          rightElement.removeFromParent()
          moveElement.removeFromParent()
        }
        
      } else {
        print("failure")
      }
    }
  }
  private func left(moveElement: CardNode){
    let positionX = (moveElement.position.x - moveElement.size.width)
    let positionY =  moveElement.position.y
    let searchPosition = CGPoint(x: positionX, y: positionY)
    if let rightElement = gameFon.atPoint(searchPosition) as? CardNode {
      if rightElement.imageNamed == moveElement.imageNamed {
        
        let textures = SKTexture.textures(FireBallsBoom.sprits())
        rightElement.animationTexture(textures: textures, time: 0.1, type: .OneRemove)
        moveElement.animationTexture(textures: textures, time: 0.1, type: .OneRemove)
        moveElement.run(SKAction.sound(file: .boom))
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
          rightElement.removeFromParent()
          moveElement.removeFromParent()
        }
        
      } else {
        print("filure")
      }
    }
  }
  private func down(moveElement: CardNode){
    let positionX = moveElement.position.x
    let positionY = (moveElement.position.y - moveElement.size.width)
    let searchPosition = CGPoint(x: positionX, y: positionY)
    if let rightElement = gameFon.atPoint(searchPosition) as? CardNode {
      if rightElement.imageNamed == moveElement.imageNamed {
        
        let textures = SKTexture.textures(FireBallsBoom.sprits())
        rightElement.animationTexture(textures: textures, time: 0.1, type: .OneRemove)
        moveElement.animationTexture(textures: textures, time: 0.1, type: .OneRemove)
        moveElement.run(SKAction.sound(file: .boom))
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
          rightElement.removeFromParent()
          moveElement.removeFromParent()
        }
        
      } else {
        print("failure")
      }
    }
  }
  private func up(moveElement: CardNode){
    let positionX = moveElement.position.x
    let positionY = (moveElement.position.y + moveElement.size.width)
    let searchPosition = CGPoint(x: positionX, y: positionY)
    if let rightElement = gameFon.atPoint(searchPosition) as? CardNode {
      if rightElement.imageNamed == moveElement.imageNamed {
        
        let textures = SKTexture.textures(FireBallsBoom.sprits())
        rightElement.animationTexture(textures: textures, time: 0.1, type: .OneRemove)
        moveElement.animationTexture(textures: textures, time: 0.1, type: .OneRemove)
        moveElement.run(SKAction.sound(file: .boom))
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
          rightElement.removeFromParent()
          moveElement.removeFromParent()
        }
        
      } else {
        print("failure")
      }
    }
  }
  init(gameScene: GameScene, gameFon: GameFonNode) {
    self.gameScene = gameScene
    self.gameFon   = gameFon
  }
}
