
import Foundation

class  ArrayCards {
  
  
  public func array(count: Int) -> [String] {
    
    let shuffled16 = [ CardsThree.array(count: count), CardsFore.array(count: count)]
    
    let shuffled9 = [Cards.array(count: count), CardsOne.array(count: count), CardsTwo.array(count: count)]
    
    switch count {
      case 4:
        return shuffled16.shuffled().first!
      case 3:
        return shuffled9.shuffled().first!
      default:
        return [String]()
    }
  
  }
}
