
import Foundation
import SpriteKit

class CheckForMatches {
  
  private let gameFon: GameFonNode!
  
  public let matchesArray: [String]!
  public var closure     : Clousure<Bool>?
  
  public func matches(count: Int){
    //Число которое является количеством ячеек
    let countCell = count
    //Размер брилианта
    let elementSquare = self.gameFon.size.height / CGFloat(countCell)
    //Расположение первого бриллианта
    let transformPositionY = (self.gameFon.position.y + (self.gameFon.size.height / 2) - (elementSquare / 2))
    let transformPositionX = (self.gameFon.position.x - (self.gameFon.size.width / 2)  + (elementSquare / 2))
    var transformPosition  = CGPoint(x: transformPositionX, y: transformPositionY)
    //Количество бриллиантов
    let elementsCounts = Array.init(repeating: Int(), count: (countCell * countCell))
    var column = 0
    var row    = 0
    var countCompare = 0
    //Создаем и располагаем бриллианта
    for (index, _) in elementsCounts.enumerated() {
      column = (index % countCell)
      
      //Получаем по координате бриллиант
      if let element = self.gameFon.atPoint(transformPosition) as? CardNode {
      
        //сравниваем его с соседями
        if self.matchesArray[index] == element.imageNamed {
          countCompare += 1
          print("success")
        } else {
          countCompare -= 1
          print("failure")
        }
      }
      
      if column == (countCell - 1) {
        transformPosition.x = transformPositionX
        transformPosition.y -= elementSquare
        row += 1
      } else {
        transformPosition.x += elementSquare
      }
     
    }
    if countCompare == matchesArray.count {
      self.closure?(true)
    } else {
      self.closure?(false)
    }
  }
  
  init(gameFon: GameFonNode, matchesArray: [String]) {
    self.gameFon      = gameFon
    self.matchesArray = matchesArray
  }
}
