//
//  GDSetupAppMetrica.swift
//  BarleyBreak
//
//  Created by Senior Developer on 09.05.2021.
//
import YandexMobileMetrica
import UserNotifications
import YandexMobileMetricaPush
import UIKit

class GDSetupAppMetrica: NSObject {
  
  public  let apyKey = "2e025b8c-8607-4672-8a17-772418e44a47"
  
  public func activate(){
    let configuration = YMMYandexMetricaConfiguration.init(apiKey: apyKey)
    YMMYandexMetrica.activate(with: configuration!)
  }
  public func setDeviceToken(deviceToken: Data){
    
    YMPYandexMetricaPush.setDeviceTokenFrom(deviceToken)
    // If the AppMetrica SDK library was not initialised before this step,
    // calling the method causes the app to crash.
    #if DEBUG
    let pushEnvironment = YMPYandexMetricaPushEnvironment.development
    #else
    let pushEnvironment = YMPYandexMetricaPushEnvironment.production
    #endif
    YMPYandexMetricaPush.setDeviceTokenFrom(deviceToken, pushEnvironment: pushEnvironment)
    let dispatchQueue = DispatchQueue(label: "appMetrica_device_id")
    //get appMetrica_device_id and after patch server
    YMMYandexMetrica.requestAppMetricaDeviceID(withCompletionQueue: dispatchQueue) { (appMetricaDeviceID, error) in
      //error
      if let error = error {
        print(error.localizedDescription, "Error get appMetrica_device_id")
        //save appMetrica_device_id
      } else {
//        guard let appMetricaDeviceID = appMetricaDeviceID else { return }
//        GVstoreProject.saveString(key: .appMetricaDeviceID, value: appMetricaDeviceID)
//        GVstoreProject.saveString(key: .deviceToken, value: deviceToken.getTokenID())
      }
    }
  }
  public func addNotification(application: UIApplication){
    
    // Register for push notifications
    // iOS 10.0 and above.
    let center = UNUserNotificationCenter.current()
    center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
      //Enable or disable features based on authorization.
    }
  }
  public func handlePushNotification(_ userInfo: [AnyHashable : Any]){
    // Track received remote notification.
    // Method [YMMYandexMetrica activateWithApiKey:] should be called before using this method.
    YMPYandexMetricaPush.handleRemoteNotification(userInfo)
  }
}

