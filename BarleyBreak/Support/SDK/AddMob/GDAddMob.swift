
import GoogleMobileAds
import Foundation
import SnapKit

class GDAddMob: NSObject {
	
	
	static let shared = GDAddMob()
	
	private let bannerID       = "ca-app-pub-4473652063394919/5104117284"
	private let nativeID       = "ca-app-pub-1071618994582157/4665524855"
  private let interstitialID = "ca-app-pub-1071618994582157/2997219960"
	
	private let testBannerID       = "ca-app-pub-3940256099942544/2934735716"
	private let testNativeID       = "ca-app-pub-3940256099942544/3986624511"
  private let testInterstitialID = "ca-app-pub-3940256099942544/4411468910"
	
  private var timer: Timer!
  
	private var bannerView: GADBannerView! = {
		let banner = GADBannerView()
    banner.adUnitID = "ca-app-pub-1847466077920040/3312985823"//"ca-app-pub-3940256099942544/2934735716"
    banner.load(GADRequest())
		return banner
	}()
  
  private var interstitial: GADInterstitialAd!
  private var adLoader = GADAdLoader()
  
	public func setup(){
		GADMobileAds.sharedInstance().start { (statusInitialization) in
			//Print status initilization
      if !statusInitialization.adapterStatusesByClassName.isEmpty {
      statusInitialization.adapterStatusesByClassName.forEach({print($0)})
      }
      print(statusInitialization.debugDescription)
      print(statusInitialization.description)
		}
		//GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["2077ef9a63d2b398840261c8221a0c9b"]
	}
	public func setupBannerView(viewController: StartGameViewController) -> GADBannerView {
    self.bannerView.delegate = viewController
    self.bannerView.rootViewController = viewController
    return self.bannerView
	}
  
}


