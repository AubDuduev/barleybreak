
import Foundation
import AppsFlyerLib

class GDAppsFlyerGetData: NSObject, AppsFlyerLibDelegate {
  
  private let parseAppsFlyerData = GDAppsFlyerParseData()
  
  public var getUrlParameters: ClousureTwo<String, Bool>!
  
  //MARK: - Public functions AppsFlyerTrackerDelegate implementation -
  public func onConversionDataSuccess(_ conversionInfo: [AnyHashable : Any]) {
    //Парсим данные из AppsFlyer
    self.parseAppsFlyerData.parseCampaign(conversionInfo)
    self.parseAppsFlyerData.parseConversionInfo(conversionInfo)
    self.parseAppsFlyerData.createURL()
    //получаем урл из данных
    self.getUrlParameters?(self.parseAppsFlyerData.url, self.parseAppsFlyerData.isCampaign)
  }
  
  public func onConversionDataFail(_ error: Error) {
    print("Error server data: class: AppsFlyerGetData ->, function: onConversionDataFail -> data: onConversionDataSuccess ->, description: ", error.localizedDescription)
  }
  
  public func onAppOpenAttribution(_ attributionData: [AnyHashable : Any]) {
    print("\(attributionData)")
  }
  public func onAppOpenAttributionFailure(_ error: Error) {
    print(error)
  }
  
  private enum KeyInstalAppsFlayer: String {
    case campaign
    case statusInstall = "af_status"
  }
  private enum ValueInstalAppsFlayer: String {
    case rating
    case organic   = "Organic"
    case noOrganic = "Non-organic"
  }
  private func keyInstalAF(_ key: KeyInstalAppsFlayer) -> String {
    return key.rawValue
  }
  private func valueInstalAF(_ key: ValueInstalAppsFlayer) -> String {
    return key.rawValue
  }
  override init(){}
}
