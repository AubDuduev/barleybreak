
import AppsFlyerLib
import Foundation

class GDAppsFlyerParseData {
  
  private var conversions = [String: String]()
  private var appCampaignWebId = String()
  private var appCampaignInfo  = String()
  private var appCampaign      = String()
  
  private var adID         = String()
  private var adSetID      = String()
  private var campaignID   = String()
  private let appsflayerID = AppsFlyerLib.shared().getAppsFlyerUID()
  
  public var url        = String()
  public var isCampaign = false
  
  public func parseCampaign(_ conversionInfo: [AnyHashable : Any]){
   
    //Проверяем есть ли параметр campaign
    if let campaign = conversionInfo["campaign"] as? String {
      
      let array = campaign.split(separator: "_")
      let webId = String((array.count > 1) ? array[1] : "")
      let info  = String((array.count > 2) ? array[2] : "")
      
      self.conversions["app_campaign_web_id"] = webId
      self.conversions["app_campaign_info"]   = info
      self.conversions["app_campaign"]        = campaign
      
      self.appCampaignWebId = webId
      self.appCampaignInfo  = info
      self.appCampaign      = campaign
      
      self.isCampaign = true
    } else {
      
      self.appCampaignWebId = "null"
      self.appCampaignInfo  = "null"
      self.appCampaign      = "null"
    }
   
  }
  
  public func parseConversionInfo(_ conversionInfo: [AnyHashable : Any]){
    
    self.adID       = conversionInfo["ad_id"]       as? String ?? "null"
    self.adSetID    = conversionInfo["adset_id"]    as? String ?? "null"
    self.campaignID = conversionInfo["campaign_id"] as? String ?? "null"
   
  }
  
  public func createURL(){
    let one   = "?app_campaign_web_id=" + self.appCampaignWebId
    let two   = "&app_campaign_info="   + self.appCampaignInfo
    let three = "&app_campaign="        + self.appCampaign
    let fore  = "&ad_id="               + self.adID
    let five  = "&adset_id="            + self.adSetID
    let six   = "&campaign_id="         + self.campaignID
    let seven = "&appsflyer_id"         + self.appsflayerID
    self.url  = one + two + three + fore + five + six + seven
  }
}
