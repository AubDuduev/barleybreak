
import AppsFlyerLib

class GDAppsFlyerSetup {
  
  public let appsFlyerGetData = GDAppsFlyerGetData()
  
  private let appsFlyerDevKey = "bvdboj2sPhgkwrFuFQZxGn"
  private let appleAppID      = "1565326806"
  
  public func setup(){
    AppsFlyerLib.shared().appsFlyerDevKey     = self.appsFlyerDevKey
    AppsFlyerLib.shared().appleAppID          = self.appleAppID
    AppsFlyerLib.shared().delegate            = self.appsFlyerGetData
    AppsFlyerLib.shared().isDebug             = true
    AppsFlyerLib.shared().useUninstallSandbox = true
  }
}

