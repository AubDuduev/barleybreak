
import Foundation
import UserNotifications
import FirebaseMessaging
import FirebaseAuth
import FirebaseAnalytics
import FirebaseCore
import FirebaseRemoteConfig

class GDSetupFirebase: NSObject {
  
  public var getSetting: Clousure<GDSettings>!
  
  public func setup(){
    FirebaseApp.configure()
  }
  
  public func setupAnalytics(isOrganic: Bool){
    Analytics.setUserProperty("non_organic", forName: "\(isOrganic)")
  }
  
  private func remoteConfig() -> RemoteConfig {
    let remoteConfig = RemoteConfig.remoteConfig()
    let settings     = RemoteConfigSettings()
    settings.minimumFetchInterval = 0
    remoteConfig.configSettings = settings
    return remoteConfig
  }
  
  public func setupRemoteConfig(){
    self.remoteConfig().fetchAndActivate { (status, error) in
      if status != .error {
        guard let dictionary = self.remoteConfig()["settings"].jsonValue as? [String: Any]          else { return }
        guard let data       = try? JSONSerialization.data(withJSONObject: dictionary, options: []) else { return }
        guard let setting    = try? JSONDecoder().decode(GDSettings.self, from: data)               else { return }
        print(setting, " - remote configuration settings")
        self.getSetting?(setting)
        if let advertisingEnd = setting.advertisingEnd {
          StoreProject.shared.saveBool(key: .advertisingEnd, value: advertisingEnd)
        } else {
          StoreProject.shared.saveBool(key: .advertisingEnd, value: true)
        }
      } else {
        
      }
    }
  }
}
