//
import SceneKit
import SpriteKit
import Foundation

class GameFonNode: SKSpriteNode {
  
  private let bitMask = BitMasks()

  public func setup(scene: SKScene) {
    let squareHeight = scene.size.height 
    self.position    = scene.position
    self.size        = CGSize(width: squareHeight, height: squareHeight)
  }
}
