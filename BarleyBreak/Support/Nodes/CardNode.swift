
import SceneKit
import SpriteKit
import Foundation

class CardNode: SKSpriteNode {

  private var gameFon  : SKSpriteNode!
  private let bitMask  = BitMasks()
  private var gameScene: GameScene!
  
  public var column    : Int!
  public var row       : Int!
  public var index     : Int!
  public var imageNamed: String!
  
  public func setup(column: Int, row: Int, index: Int, imageNamed: String?, scene: GameScene) {
    self.imageNamed = imageNamed
    self.column     = column
    self.row        = row
    self.index      = index
    self.gameScene  = scene
  }
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
    self.gameScene.moveElements(elementTouch: self)
  }
  public func light(color: UIColor){
    self.color   = .white
    let colorOne = SKAction.colorize(with: color , colorBlendFactor: 1.0, duration: 0.2)
    let colorTwo = SKAction.colorize(with: .white, colorBlendFactor: 1.0, duration: 0.2)
    let group    = SKAction.group([colorOne, colorTwo, colorOne, colorTwo])
    let repeatAC = SKAction.repeat(group, count: 5)
    self.run(repeatAC){
      self.run(colorTwo)
    }
  }
  override init(texture: SKTexture?, color: UIColor, size: CGSize) {
    super.init(texture: texture, color: color, size: size)
  }
  convenience init(imageNamed: String, position: CGPoint, elementSquare: CGFloat) {
    let texture = SKTexture(imageNamed: imageNamed)
    self.init(texture: texture, color: .clear, size: .zero)
    self.position    = position
    self.size        = CGSize(width: elementSquare, height: elementSquare)
    self.physicsBody = SKPhysicsBody(circleOfRadius: (self.size.width / 2))
    self.isUserInteractionEnabled = true
    self.physicsBody?.isDynamic   = false
    self.physicsBody?.friction    = 0
    self.physicsBody?.restitution = 0
//    self.physicsBody?.categoryBitMask    = self.bitMask.value(.box)
//    self.physicsBody?.contactTestBitMask = self.bitMask.value(.border) | self.bitMask.value(.box)
//    self.physicsBody?.collisionBitMask   = self.bitMask.value(.border) | self.bitMask.value(.box)
    self.physicsBody?.affectedByGravity  = false
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
