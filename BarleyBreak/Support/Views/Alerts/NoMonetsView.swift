//
//  NoMonetsView.swift
//  BarleyBreak
//
//  Created by Senior Developer on 06.05.2021.
//
import SwiftEntryKit
import UIKit

class NoMonetsView: UIView {
  
  public var attributes = EKAttributes()
  public var callback   : ClousureEmpty!
  
  @IBOutlet weak var alertTextLabel : UILabel!
  @IBOutlet weak var buttonStackView: UIStackView!

  public func attributesSetup(){
    self.cornerRadius(7, true)
    self.shadowColor(color: .black, radius: 2)
    self.attributes.name              = self.debugDescription
    self.attributes.displayMode       = .dark
    self.attributes.shadow            = .active(with: .init(opacity: 3, radius: 5))
    self.attributes.position          = .center
    self.attributes.displayDuration   = .infinity
    let widthConstraint               = EKAttributes.PositionConstraints.Edge.constant(value: 543)
    let heightConstraint              = EKAttributes.PositionConstraints.Edge.constant(value: 338)
    self.attributes.positionConstraints.size = .init(width: widthConstraint, height: heightConstraint)
    self.attributes.roundCorners      = .all(radius: 7)
    self.attributes.screenInteraction = .dismiss
    self.attributes.entryInteraction  = .absorbTouches
    self.attributes.scroll            = .enabled(swipeable: false, pullbackAnimation: .jolt)
    let offset = EKAttributes.PositionConstraints.KeyboardRelation.Offset(bottom: 70, screenEdgeResistance: 50)
    let keyboardRelation = EKAttributes.PositionConstraints.KeyboardRelation.bind(offset: offset)
    self.attributes.positionConstraints.keyboardRelation = keyboardRelation
  }
  override func layoutSubviews() {
    super.layoutSubviews()
    self.frame.size = CGSize(width: 543, height: 338)
  }
  public func set(type: AlertType){
    
    switch type {
      
      case .noMonet:
        self.alertTextLabel.text = type.rawValue
        self.buttonStackView.isHidden = true
      case .youPay:
        self.alertTextLabel.text = type.rawValue
        self.buttonStackView.isHidden = false
    }
  }
  
  enum AlertType: String {
    
    case noMonet = "У Вас недостаточно монет"
    case youPay  = "Вы хотите купить эту картинку?"
  }
  
  @IBAction func okButton(button: UIButton){
    self.callback?()
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
      SwiftEntryKit.dismiss()
    }
  }
  
  @IBAction func cancelButton(button: UIButton){
    SwiftEntryKit.dismiss()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.attributesSetup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
  }
}
