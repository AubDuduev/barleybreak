
import UIKit
import Foundation

class NoBonusView: UIView {
  
  public var continueClosure: Clousure<Bool>!
  
  @IBAction func continueButton(button: UIButton){
    self.continueClosure(true)
  }
}
