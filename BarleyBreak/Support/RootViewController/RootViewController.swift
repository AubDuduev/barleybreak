
import UIKit

class RootVC {
  
  public func set(window: UIWindow?, root: UIStoryboard.ID){
    let rootViewController = UIStoryboard.createVC(sbID: root)
    window?.rootViewController = rootViewController
    window?.makeKeyAndVisible()
  }
}

