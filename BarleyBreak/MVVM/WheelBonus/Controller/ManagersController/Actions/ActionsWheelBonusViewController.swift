//
//  ActionsWheelBonusViewController.swift
//  BookOfRaSlots
//
//  Created by Senior Developer on 09.12.2020.
//
import UIKit

extension WheelBonusViewController {
  
  @IBAction func backButton(button: UIButton){
    self.dismiss(animated: true)
  }
  @IBAction func spinWheelButton(button: UIButton){
    self.controllers.logic.spinWheel()
  }
}
