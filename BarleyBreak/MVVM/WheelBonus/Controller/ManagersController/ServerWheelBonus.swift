//
//  ServerWheelBonus.swift
//  BookOfRaSlots

import Foundation

class ServerWheelBonus: Controller {
  
  //MARK: - Public variable
  public var VC: WheelBonusViewController!
  
  
}
//MARK: - Initial
extension ServerWheelBonus{
  
  //MARK: - Inition
  convenience init(viewController: WheelBonusViewController) {
    self.init()
    self.VC = viewController
  }
}

