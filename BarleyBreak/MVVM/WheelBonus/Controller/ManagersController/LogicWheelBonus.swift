//
//  LogicWheelBonus.swift
//  BookOfRaSlots
import UIKit
import SpriteKit

class LogicWheelBonus: Controller {
  
  //MARK: - Public variable
  public var VC: WheelBonusViewController!
  
  public func spinWheel(){
    guard self.VC.controllers.present.trySpin() else {
      self.VC.dismiss(animated: true, completion: nil)
      return
    }
    self.VC.wheelView.spin()
    
    //Set wheel number section
    let randomIndex: Int = .random(in: 0...5)
    let gradus = CGFloat(self.VC.arrayBonus[randomIndex])
    let sum    = self.VC.arraySumm[randomIndex]
    GDAudioPlayer.shared.load(fileName: .roulette, type: .mp3)
    GDAudioPlayer.shared.play()
    //Set randomIndex gradus wheel
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
      guard let self = self else { return }
      self.VC.wheelView.transform = CGAffineTransform(rotationAngle: .radians(gr: gradus))
      
      //Set music game
      DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
        guard let self = self else { return }
        GDAudioPlayer.shared.load(fileName: .winRoulette, type: .mp3)
        GDAudioPlayer.shared.play()
        self.setData(sum: sum)
        self.arrayElements(randomIndex: randomIndex)
        
        GDAudioPlayer.shared.pause(delay: 2)
      }
    }
  }
  private func setData(sum: Int){
    let currentSumm = StoreProject.shared.getInt(key: .currentScore) ?? 0
    let result = currentSumm + sum
    
    self.VC.balanceLabel.text = String(result)
    self.VC.winLabel.text     = String(sum)
    
    StoreProject.shared.saveInt(key: .currentScore, value: result)
  }
  public func spin(){
    self.VC.currentBonus += 5
    self.VC.wheelView.transform = CGAffineTransform(rotationAngle: .radians(gr:  CGFloat(self.VC.currentBonus)))
    self.VC.balanceLabel.text = String(self.VC.currentBonus)
  }
  public func arrayElements(randomIndex: Int){
    self.VC.winsLabels.forEach({$0.textColor = .white})
    self.VC.winsImages.forEach({$0.transform = CGAffineTransform(scaleX: 1, y: 1)})
    
    self.VC.winsLabels[randomIndex].textColor = .red
    self.VC.winsImages[randomIndex].transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
  }
  
}
//MARK: - Initial
extension LogicWheelBonus{
  
  //MARK: - Inition
  convenience init(viewController: WheelBonusViewController) {
    self.init()
    self.VC = viewController
  }
}

