//
//  PresentWheelBonus.swift
//  BookOfRaSlots

import Foundation

class PresentWheelBonus: Controller {
  
  //MARK: - Public variable
  public var VC: WheelBonusViewController!
  
  public func viewDidLoad() {
    self.setData()
    _ = self.trySpin()
  }
  
  private func setData(){
    let currentSumm = StoreProject.shared.getInt(key: .currentScore) ?? 0
    self.VC.balanceLabel.text = String(currentSumm)
  }
  
  public func trySpin() -> Bool {
    guard self.VC.currentTrys != 0 else { return false }
    self.VC.currentTrys -= 1
    self.VC.tryButton.setTitle(String(self.VC.currentTrys), for: .normal)
    return true
  }
}
//MARK: - Initial
extension PresentWheelBonus{
  
  //MARK: - Inition
  convenience init(viewController: WheelBonusViewController) {
    self.init()
    self.VC = viewController
  }
}

