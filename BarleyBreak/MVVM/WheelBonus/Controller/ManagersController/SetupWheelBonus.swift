//
//  SetupWheelBonus.swift
//  BookOfRaSlots

import Foundation

class SetupWheelBonus: Controller {
  
  //MARK: - Public variable
  public var VC: WheelBonusViewController!
  
  
}
//MARK: - Initial
extension SetupWheelBonus{
  
  //MARK: - Inition
  convenience init(viewController: WheelBonusViewController) {
    self.init()
    self.VC = viewController
  }
}

