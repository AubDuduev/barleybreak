//
//  WheelBonusViewController.swift

import UIKit

class WheelBonusViewController: UIViewController {
  
  //MARK: - Controllers
  public var controllers: WheelBonusControllers!
  
  //MARK: - Public variable
  
  public var arrayBonus   = [55, 300, 125, 0, 175, 240]//крест, алмаз, доллар, глаз, цветок, жук
  public var arraySumm    = [100, 200, 300, 400, 500, 1000]
  public var currentBonus = 0
  public var currentTrys  = 4
  
  //MARK: - Outlets
  @IBOutlet weak var wheelView   : WheelView!
  @IBOutlet weak var balanceLabel: UILabel!
  @IBOutlet weak var winLabel    : UILabel!
  @IBOutlet weak var tryButton   : UIButton!
  
  //MARK: - Outlets array
  @IBOutlet var winsLabels: [UILabel]!
  @IBOutlet var winsImages: [UIImageView]!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setControllers()
    self.controllers.present.viewDidLoad()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
  }
  private func setControllers(){
    self.controllers = WheelBonusControllers(viewController: self)
  }
}
