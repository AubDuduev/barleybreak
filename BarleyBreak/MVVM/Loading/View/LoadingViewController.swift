import UIKit

class LoadingViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: LoadingViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
    self.viewModel.viewDidLoad()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.viewModel.viewDidAppear()
  }
  private func initViewModel(){
    self.viewModel = LoadingViewModel(viewController: self)
  }
}
