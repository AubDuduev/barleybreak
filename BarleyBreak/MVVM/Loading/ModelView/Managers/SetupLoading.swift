
import UIKit

class SetupLoading: VMManager {
  
  //MARK: - Public variable
  public var VM: LoadingViewModel!
  
  public func setupMusic(){
    GDAudioPlayer.shared.load(fileName: .background2, type: .mp3)
    GDAudioPlayer.shared.play()
  }
  public func scoreStart(){
    if StoreProject.shared.getInt(key: .currentScore) == nil {
      StoreProject.shared.saveInt(key: .currentScore, value: 3000)
    }
    if StoreProject.shared.getInt(key: .currentScore) == 0 {
      StoreProject.shared.saveInt(key: .currentScore, value: 3000)
    }
  }
}
//MARK: - Initial
extension SetupLoading {
  
  //MARK: - Inition
  convenience init(viewModel: LoadingViewModel) {
    self.init()
    self.VM = viewModel
  }
}


