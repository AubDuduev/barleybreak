
import UIKit

class RouterLoading: VMManager {
  
  //MARK: - Public variable
  public var VM: LoadingViewModel!
  
  
  public func push(_ type: Push){
    
    switch type {
      case .StartGameVC:
        self.pushStartGameVC()
    }
  }
  
  enum Push {
    case StartGameVC
  }
}
//MARK: - Private functions
extension RouterLoading {
  
  private func pushStartGameVC(){
    let startGameVC = self.VM.VC.getVCForID(storyboardID     : .StartGame,
                                            vcID             : .StartGameVC,
                                            transitionStyle  : .crossDissolve,
                                            presentationStyle: .fullScreen) as! StartGameViewController
    self.VM.VC.present(startGameVC, animated: true)
  }
}
//MARK: - Initial
extension RouterLoading {
  
  //MARK: - Inition
  convenience init(viewModel: LoadingViewModel) {
    self.init()
    self.VM = viewModel
  }
}



