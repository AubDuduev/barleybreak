
import UIKit

enum MenuModel {
	
	case loading
	case getData
	case errorHandler(MenuData?)
	case presentData(MenuData)
	
	
}

