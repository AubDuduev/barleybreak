
import Foundation

class MenuManagers: VMManagers {
  
  let setup    : SetupMenu!
  let server   : ServerMenu!
  let present  : PresentMenu!
  let logic    : LogicMenu!
  let animation: AnimationMenu!
  let router   : RouterMenu!
  
  init(viewModel: MenuViewModel) {
    
    self.setup     = SetupMenu(viewModel: viewModel)
    self.server    = ServerMenu(viewModel: viewModel)
    self.present   = PresentMenu(viewModel: viewModel)
    self.logic     = LogicMenu(viewModel: viewModel)
    self.animation = AnimationMenu(viewModel: viewModel)
    self.router    = RouterMenu(viewModel: viewModel)
  }
}

