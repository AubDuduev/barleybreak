
import Foundation

class MenuViewModel: VMManagers {
  
  public var menuModel: MenuModel = .loading {
    didSet{
      self.logicMenuModel()
    }
  }
  
  //MARK: - Public variable
  public var managers: MenuManagers!
  public var VC      : MenuViewController!
  
  
  func viewWillAppear() {
    self.managers.present.setPlayLogo()
  }
  
  public func viewDidLoad(){
    self.managers.setup.soundOnOffImageView()
    self.managers.setup.musicOnOffImageView()
    self.managers.setup.balanceLabel()
    self.managers.setup.logoImageView()
  }
  
  public func logicMenuModel(){
    
    switch self.menuModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
}
//MARK: - Initial
extension MenuViewModel {
  
  convenience init(viewController: MenuViewController) {
    self.init()
    self.VC       = viewController
    self.managers = MenuManagers(viewModel: self)
  }
}
