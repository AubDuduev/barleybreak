
import UIKit

extension MenuViewController {
  
  @IBAction func backButton(button: UIButton){
    self.dismiss(animated: true)
  }
  
  @IBAction func soundSwipeGesture(gesture: UISwipeGestureRecognizer){
    self.viewModel.managers.logic.swipeGameSound(gesture: gesture)
  }
  @IBAction func musicSwipeGesture(gesture: UISwipeGestureRecognizer){
    self.viewModel.managers.logic.swipeGameMusic(gesture: gesture)
  }
}
