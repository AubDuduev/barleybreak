
import UIKit

class SetupMenu: VMManager {
  
  //MARK: - Public variable
  public var VM: MenuViewModel!
  
  public func soundOnOffImageView(){
    if !StoreProject.shared.getBool(key: .isSound) {
      let swipe = UISwipeGestureRecognizer()
      swipe.direction = .left
      self.VM.managers.logic.swipeGameSound(gesture: swipe)
    } else {
      let swipe = UISwipeGestureRecognizer()
      swipe.direction = .right
      self.VM.managers.logic.swipeGameSound(gesture: swipe)
    }
  }
  public func musicOnOffImageView(){
    if !StoreProject.shared.getBool(key: .isMusic) {
      let swipe = UISwipeGestureRecognizer()
      swipe.direction = .left
      self.VM.managers.logic.swipeGameMusic(gesture: swipe)
    } else {
      let swipe = UISwipeGestureRecognizer()
      swipe.direction = .right
      self.VM.managers.logic.swipeGameMusic(gesture: swipe)
    }
  }
  public func balanceLabel(){
    Score.shared.setBalance(label: self.VM.VC.balanceLabel)
  }
  public func logoImageView(){
    self.VM.VC.logoImageView.shadowColor(color: .black, radius: 20)
  }
}
//MARK: - Initial
extension SetupMenu {
  
  //MARK: - Inition
  convenience init(viewModel: MenuViewModel) {
    self.init()
    self.VM = viewModel
  }
}


