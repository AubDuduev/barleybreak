import Foundation
import UIKit

class LogicMenu: VMManager {
  
  //MARK: - Public variable
  public var VM: MenuViewModel!
  
  public func swipeGameSound(gesture: UISwipeGestureRecognizer){
    UIView.animate(withDuration: 0.3) {
      let swipeWidth: CGFloat = 22
      if gesture.direction == .left {
        StoreProject.shared.saveBool(key: .isSound, value: false)
        self.VM.VC.soundOnOffImageViewCenterConstant.constant = -swipeWidth
      } else if gesture.direction == .right {
        StoreProject.shared.saveBool(key: .isSound, value: true)
        self.VM.VC.soundOnOffImageViewCenterConstant.constant = swipeWidth
      }
      self.VM.VC.view.layoutIfNeeded()
    }
  }
  public func swipeGameMusic(gesture: UISwipeGestureRecognizer){
    UIView.animate(withDuration: 0.3) {
      let swipeWidth: CGFloat = 22
      if gesture.direction == .left {
        StoreProject.shared.saveBool(key: .isMusic, value: false)
        self.VM.VC.musicOnOffImageViewCenterConstant.constant = -swipeWidth
        GDAudioPlayer.shared.pause()
      } else if gesture.direction == .right {
        StoreProject.shared.saveBool(key: .isMusic, value: true)
        self.VM.VC.musicOnOffImageViewCenterConstant.constant = swipeWidth
        GDAudioPlayer.shared.load(fileName: .background2, type: .mp3)
        GDAudioPlayer.shared.play()
      }
      self.VM.VC.view.layoutIfNeeded()
    }
  }
  
}
//MARK: - Initial
extension LogicMenu {
  
  //MARK: - Inition
  convenience init(viewModel: MenuViewModel) {
    self.init()
    self.VM = viewModel
  }
}
