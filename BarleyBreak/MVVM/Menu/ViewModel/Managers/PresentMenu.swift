
import UIKit

class PresentMenu: VMManager {
  
  //MARK: - Public variable
  public var VM: MenuViewModel!
  
  public func setPlayLogo(){
    var playLogo = StoreProject.shared.getString(key: .payLogo)
    playLogo = (playLogo == "") ? "logoGame" : playLogo
    self.VM.VC.playLogoImageView.image = UIImage(named: playLogo)
  }
}
//MARK: - Initial
extension PresentMenu {
  
  //MARK: - Inition
  convenience init(viewModel: MenuViewModel) {
    self.init()
    self.VM = viewModel
  }
}

