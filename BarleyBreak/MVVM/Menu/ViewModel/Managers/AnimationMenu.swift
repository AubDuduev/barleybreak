import UIKit

class AnimationMenu: VMManager {
  
  //MARK: - Public variable
  public var VM: MenuViewModel!
  
  
}
//MARK: - Initial
extension AnimationMenu {
  
  //MARK: - Inition
  convenience init(viewModel: MenuViewModel) {
    self.init()
    self.VM = viewModel
  }
}

