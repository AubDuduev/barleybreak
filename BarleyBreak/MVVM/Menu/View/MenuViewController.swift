import UIKit

class MenuViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: MenuViewModel!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
  @IBOutlet weak var logoImageView      : UIImageView!
  @IBOutlet weak var soundOnOffImageView: UIImageView!
  @IBOutlet weak var musicOnOffImageView: UIImageView!
  @IBOutlet weak var playLogoImageView  : UIImageView!
  @IBOutlet weak var balanceLabel       : UILabel!
  
  //MARK: - Outlets Constant
  @IBOutlet weak var soundOnOffImageViewCenterConstant: NSLayoutConstraint!
  @IBOutlet weak var musicOnOffImageViewCenterConstant: NSLayoutConstraint!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
    self.viewModel.viewDidLoad()
  }
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.viewModel.viewWillAppear()
  }
  private func initViewModel(){
    self.viewModel = MenuViewModel(viewController: self)
  }
}
