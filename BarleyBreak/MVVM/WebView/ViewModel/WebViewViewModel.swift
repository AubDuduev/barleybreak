
import Foundation

class WebViewViewModel: VMManagers {
  
  public var webViewModal: WebViewModel = .loading {
    didSet{
      self.logicWebViewModel()
    }
  }
  
  //MARK: - Public variable
  public var managers: WebViewManagers!
  public var VC      : WebViewViewController!
  
  public func viewDidLoad() {
    self.webViewModal = .loading
    self.managers.setup.backButton(isEnabled: StoreProject.shared.getBool(key: .advertisingEnd))
    self.managers.setup.orientation()
  }
  public func viewDidAppear() {
    self.managers.setup.orientation()
  }
  public func logicWebViewModel(){
    
    switch self.webViewModal {
      //1 - Загрузка
      case .loading:
        self.managers.setup.setupWebView()
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
  
}
//MARK: - Initial
extension WebViewViewModel {
  
  convenience init(viewController: WebViewViewController) {
    self.init()
    self.VC       = viewController
    self.managers = WebViewManagers(viewModel: self)
  }
}
