
import UIKit

extension WebViewViewController {
  
  @IBAction func backButton(button: UIButton){
    self.pushVCForID(storyboardID: .StartGame ,
                     vcID: .StartGameVC,
                     animation: true,
                     transitionStyle: .crossDissolve,
                     presentationStyle: .fullScreen)
  }
}
