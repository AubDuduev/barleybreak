
import UIKit
import WebKit
import SnapKit

class SetupWebView: VMManager {
   
   //MARK: - Public variable
   public var VM: WebViewViewModel!
   
   private let group = DispatchGroup()
   
   public func setupWebView(){
      let urlString = StoreProject.shared.getString(key: .mainURL)
      //Настройки cookies
      self.setupConfigurationWkWebView(){ webViewConfig in
         //получение урл с настройками
         self.getSetupCookiesURL(from: urlString) { response in
            webViewConfig.set(string: response?.url?.absoluteString)
         }
      }
     // self.webViewOld()
      StoreProject.shared.saveBool(key: .isOpenURL, value: true)
   }
   public func orientation(){
      Orientation.lockOrientation(.all)
   }
   public func backButton(isEnabled: Bool){
      self.VM.VC.backButton.isHidden = isEnabled 
   }
   public func setupConfigurationWkWebView(completion: @escaping Clousure<WKWebView>){
      //Конфигурации
      let configuration = WKWebViewConfiguration()
      let controller    = WKUserContentController()
      configuration.userContentController = controller
      configuration.preferences.javaScriptCanOpenWindowsAutomatically = true
      configuration.preferences.javaScriptEnabled = true
      //Хранилище данных
      let wkDataStore = WKWebsiteDataStore.nonPersistent()
      //wkDataStore.removeData(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes(), modifiedSince: .distantPast) { }
      
      let cookies = HTTPCookieStorage.shared.cookies
      cookies?.forEach { wkDataStore.httpCookieStore.setCookie($0) }
      
      for cookie in cookies ?? [] {
         self.group.enter()
         wkDataStore.httpCookieStore.setCookie(cookie) {
            self.group.leave()
         }
      }
      configuration.websiteDataStore = wkDataStore
      //Создаем вебвью
      self.group.notify(queue: .main) {
         
         let webView = WKWebView(frame: .zero, configuration: configuration)
         webView.navigationDelegate = self.VM.VC
         webView.uiDelegate         = self.VM.VC
         webView.backgroundColor    = .white
         self.VM.VC.view.addSubview(webView)
         webView.snp.makeConstraints { make in
            make.edges.equalTo(self.VM.VC.view)
         }
         completion(webView)
      }
   }
   private func webViewOld(){
      let urlString = StoreProject.shared.getString(key: .mainURL)
      guard let url = URL(string: urlString.encodedUrl()!) else { return }
      let webView = UIWebView(frame: self.VM.VC.view.frame)
      webView.delegate = self.VM.VC
      var request = URLRequest(url: url)
      if let cookies = HTTPCookieStorage.shared.cookies(for: url) {
         let headers = HTTPCookie.requestHeaderFields(with: cookies)
         headers.forEach({ request.addValue($0.value, forHTTPHeaderField: $0.key) })
         webView.loadRequest(request)
      } else {
         webView.loadRequest(request)
      }
      
      
      self.VM.VC.view.addSubview(webView)
      webView.snp.makeConstraints { make in
         make.edges.equalTo(self.VM.VC.view)
      }
   }
   func getSetupCookiesURL(from urlString: String?, completion: @escaping Clousure<URLResponse?>){
      guard let urlString = urlString else { return }
      guard let url = URL(string: urlString) else { return }
      let configuration = URLSessionConfiguration.default
      configuration.httpCookieAcceptPolicy = .always
      configuration.httpShouldSetCookies   = true
      configuration.httpCookieStorage?.cookieAcceptPolicy = .always
      let session = URLSession(configuration: configuration)
      session.dataTask(with: url) { _, response, _ in
         DispatchQueue.main.async {
            completion(response)
         }
      }.resume()
   }
}
//MARK: - Initial
extension SetupWebView {
   
   //MARK: - Inition
   convenience init(viewModel: WebViewViewModel) {
      self.init()
      self.VM = viewModel
   }
}




