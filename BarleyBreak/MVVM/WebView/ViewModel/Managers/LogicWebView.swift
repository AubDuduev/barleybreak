import Foundation

class LogicWebView: VMManager {
  
  //MARK: - Public variable
  public var VM: WebViewViewModel!
  
  
}
//MARK: - Initial
extension LogicWebView {
  
  //MARK: - Inition
  convenience init(viewModel: WebViewViewModel) {
    self.init()
    self.VM = viewModel
  }
}
