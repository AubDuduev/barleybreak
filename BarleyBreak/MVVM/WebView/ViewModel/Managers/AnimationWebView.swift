import UIKit

class AnimationWebView: VMManager {
  
  //MARK: - Public variable
  public var VM: WebViewViewModel!
  
  
}
//MARK: - Initial
extension AnimationWebView {
  
  //MARK: - Inition
  convenience init(viewModel: WebViewViewModel) {
    self.init()
    self.VM = viewModel
  }
}

