
import UIKit

class PresentWebView: VMManager {
  
  //MARK: - Public variable
  public var VM: WebViewViewModel!
  
  
}
//MARK: - Initial
extension PresentWebView {
  
  //MARK: - Inition
  convenience init(viewModel: WebViewViewModel) {
    self.init()
    self.VM = viewModel
  }
}

