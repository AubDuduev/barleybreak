import UIKit

class ServerWebView: VMManager {
  
  //MARK: - Public variable
  public var VM: WebViewViewModel!
  
  
}
//MARK: - Initial
extension ServerWebView {
  
  //MARK: - Inition
  convenience init(viewModel: WebViewViewModel) {
    self.init()
    self.VM = viewModel
  }
}


