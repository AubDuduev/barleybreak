//
//  WebViewWebContentDelegate.swift
import UIKit
import WebKit

extension WebViewViewController: WKNavigationDelegate, WKUIDelegate, UIWebViewDelegate {
   
   func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
      SetupEventsWeb.shared.testURL(url: webView.url)
      SetupEventsWeb.shared.dismiss(url: webView.url, webVC: self)
   }
   func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
      
   }
   func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
      guard let response = (navigationResponse.response as? HTTPURLResponse) else { return decisionHandler(.allow) }
      guard response.statusCode == 404 else { return decisionHandler(.allow) }
      SetupEventsWeb.shared.dismiss(url: webView.url, webVC: self)
      self.viewModel.managers.setup.backButton(isEnabled: false)
      decisionHandler(.allow)
   }
}
func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
   
   guard let response = navigationResponse.response as? HTTPURLResponse,
         let url = navigationResponse.response.url else {
      decisionHandler(.cancel)
      return
   }
   
   if let headerFields = response.allHeaderFields as? [String: String] {
      let cookies = HTTPCookie.cookies(withResponseHeaderFields: headerFields, for: url)
      cookies.forEach { cookie in
         webView.configuration.websiteDataStore.httpCookieStore.setCookie(cookie)
      }
   }
   
   decisionHandler(.allow)
}
