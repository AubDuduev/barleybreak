
import UIKit

enum WebViewModel {
	
	case loading
	case getData
	case errorHandler(WebViewData?)
	case presentData(WebViewData)
	
	
}

