
import Foundation

class WebViewManagers: VMManagers {
  
  let setup    : SetupWebView!
  let server   : ServerWebView!
  let present  : PresentWebView!
  let logic    : LogicWebView!
  let animation: AnimationWebView!
  let router   : RouterWebView!
  
  init(viewModel: WebViewViewModel) {
    
    self.setup     = SetupWebView(viewModel: viewModel)
    self.server    = ServerWebView(viewModel: viewModel)
    self.present   = PresentWebView(viewModel: viewModel)
    self.logic     = LogicWebView(viewModel: viewModel)
    self.animation = AnimationWebView(viewModel: viewModel)
    self.router    = RouterWebView(viewModel: viewModel)
  }
}

