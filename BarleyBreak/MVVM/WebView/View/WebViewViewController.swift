import UIKit
import WebKit

class WebViewViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: WebViewViewModel!
  
  //MARK: - Outlets
  @IBOutlet var contentWebView: WKWebView!
  @IBOutlet weak var backButton    : UIButton!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
    self.viewModel.viewDidLoad()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.viewModel.viewDidAppear()
  }
  private func initViewModel(){
    self.viewModel = WebViewViewModel(viewController: self)
  }
}
