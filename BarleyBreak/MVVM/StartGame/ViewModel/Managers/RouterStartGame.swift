
import UIKit

class RouterStartGame: VMManager {
  
  //MARK: - Public variable
  public var VM: StartGameViewModel!
  
  public func push(_ type: Push){
    
    switch type {
      
      case .GameVC:
        self.pushGame()
      case .WheelBonus:
        self.pushWheelBonusVC()
      case .PayLogo:
        self.pushPayLogoVC()
    }
  }
  
  enum Push {
    case GameVC
    case WheelBonus
    case PayLogo
  }
}
//MARK: - Private functions
extension RouterStartGame {
  
  private func pushGame(){
    let gameVC = self.VM.VC.getVCForID(storyboardID     : .Game,
                                       vcID             : .GameVC,
                                       transitionStyle  : .crossDissolve,
                                       presentationStyle: .fullScreen) as! GameViewController
    self.VM.VC.present(gameVC, animated: true)
  }
  private func pushWheelBonusVC(){
    let wheelBonusVC = self.VM.VC.getVCForID(storyboardID     : .WheelBonus,
                                             vcID             : .WheelBonusVC,
                                             transitionStyle  : .crossDissolve,
                                             presentationStyle: .fullScreen) as! WheelBonusViewController
    self.VM.VC.present(wheelBonusVC, animated: true)
  }
  private func pushPayLogoVC(){
    let payLogoVC = self.VM.VC.getVCForID(storyboardID     : .PayLogo,
                                          vcID             : .PayLogoVC,
                                          transitionStyle  : .crossDissolve,
                                          presentationStyle: .fullScreen) as! PayLogoViewController
    self.VM.VC.present(payLogoVC, animated: true)
  }
}
//MARK: - Initial
extension RouterStartGame {
  
  //MARK: - Inition
  convenience init(viewModel: StartGameViewModel) {
    self.init()
    self.VM = viewModel
  }
}



