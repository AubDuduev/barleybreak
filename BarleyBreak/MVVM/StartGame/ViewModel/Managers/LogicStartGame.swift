import Foundation

class LogicStartGame: VMManager {
  
  //MARK: - Public variable
  public var VM: StartGameViewModel!
  
  public func gameNew(){
    self.VM.managers.router.push(.GameVC)
  }
  public func gameContinue(){
    self.VM.managers.router.push(.GameVC)
  }
  public func wheelBonus(){
    if BonusDay.shared.isBonus() {
      self.VM.managers.router.push(.WheelBonus)
    } else {
      self.noBonusViewIsHidden(is: false)
    }
  }
  public func noBonusViewIsHidden(is hidden: Bool){
    self.VM.noBonusView.isHidden = hidden
  }
  public func payLogo(){
    self.VM.managers.router.push(.PayLogo)
  }
  
}
//MARK: - Initial
extension LogicStartGame {
  
  //MARK: - Inition
  convenience init(viewModel: StartGameViewModel) {
    self.init()
    self.VM = viewModel
  }
}
