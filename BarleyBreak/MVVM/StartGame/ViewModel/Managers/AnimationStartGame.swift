import UIKit

class AnimationStartGame: VMManager {
  
  //MARK: - Public variable
  public var VM: StartGameViewModel!
  
  
}
//MARK: - Initial
extension AnimationStartGame {
  
  //MARK: - Inition
  convenience init(viewModel: StartGameViewModel) {
    self.init()
    self.VM = viewModel
  }
}

