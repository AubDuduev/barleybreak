
import UIKit

extension StartGameViewController {
  
  @IBAction func gameNewButton(button: UIButton){
    self.viewModel.managers.logic.gameNew()
  }
  @IBAction func gameContinueButton(button: UIButton){
    self.viewModel.managers.logic.gameContinue()
  }
  @IBAction func wheelBonusButton(button: UIButton){
     self.viewModel.managers.logic.wheelBonus()
  }
  @IBAction func payLogoButton(button: UIButton){
     self.viewModel.managers.logic.payLogo()
  }
}
