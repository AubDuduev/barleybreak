
import UIKit
import SnapKit

class SetupStartGame: VMManager {
  
  //MARK: - Public variable
  public var VM: StartGameViewModel!
  
  public func balanceLabel(){
    Score.shared.setBalance(label: self.VM.VC.balanceLabel)
  }
  public func logoImageView(){
    self.VM.VC.logoImageView.shadowColor(color: .black, radius: 20)
  }
  public func noBonusView(){
    self.VM.noBonusView.snp.makeConstraints { (noBonusView) in
      noBonusView.edges.equalTo(self.VM.VC.view)
    }
    self.VM.managers.logic.noBonusViewIsHidden(is: true)
    self.VM.noBonusView.continueClosure = { [weak self] success in
      self?.VM.managers.logic.noBonusViewIsHidden(is: success)
    }
  }
  public func orientation(){
    Orientation.lockOrientation(.landscape)
  }
  public func addedView(){
    self.VM.VC.view.addSubview(self.VM.noBonusView)
  }
  public func bannerAdd(){
    let bannerView = GDAddMob.shared.setupBannerView(viewController: self.VM.VC)
    self.VM.VC.view.addSubview(bannerView)
    bannerView.snp.makeConstraints { make in
      make.left.equalTo(self.VM.VC.view).offset(0)
      make.right.equalTo(self.VM.VC.view).offset(0)
      make.bottom.equalTo(self.VM.VC.view).offset(0)
      make.height.equalTo(60)
    }
  }
}
//MARK: - Initial
extension SetupStartGame {
  
  //MARK: - Inition
  convenience init(viewModel: StartGameViewModel) {
    self.init()
    self.VM = viewModel
  }
}


