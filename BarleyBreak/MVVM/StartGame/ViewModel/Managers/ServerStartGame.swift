import UIKit

class ServerStartGame: VMManager {
  
  //MARK: - Public variable
  public var VM: StartGameViewModel!
  
  
}
//MARK: - Initial
extension ServerStartGame {
  
  //MARK: - Inition
  convenience init(viewModel: StartGameViewModel) {
    self.init()
    self.VM = viewModel
  }
}


