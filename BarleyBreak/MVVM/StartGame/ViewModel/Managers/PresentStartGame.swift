
import UIKit

class PresentStartGame: VMManager {
  
  //MARK: - Public variable
  public var VM: StartGameViewModel!
  
  public func setPlayLogo(){
    var playLogo = StoreProject.shared.getString(key: .payLogo)
    playLogo = (playLogo == "") ? "logoGame" : playLogo
    self.VM.VC.playLogoImageView.image = UIImage(named: playLogo)
  }
}
//MARK: - Initial
extension PresentStartGame {
  
  //MARK: - Inition
  convenience init(viewModel: StartGameViewModel) {
    self.init()
    self.VM = viewModel
  }
}

