
import UIKit
import Foundation

class StartGameViewModel: VMManagers {
	
	public var startGameModel: StartGameModel = .loading {
		didSet{
			self.logicLoadingModel()
		}
	}
  
  //MARK: - Public variable
  public var noBonusView = NoBonusView().loadNib()
  public var managers    : StartGameManagers!
  public var VC          : StartGameViewController!
  
  public func viewDidLoad() {
    self.managers.setup.addedView()
    self.managers.setup.orientation()
  }
  public func viewDidLayoutSubviews() {
    self.managers.setup.noBonusView()
  }
  public func viewDidAppear() {
    self.managers.setup.orientation()
  }
  public func viewWillAppear() {
    self.managers.setup.balanceLabel()
    self.managers.setup.logoImageView()
    self.managers.present.setPlayLogo()
    self.managers.setup.bannerAdd()
  }
 
  public func logicLoadingModel(){
    
    switch self.startGameModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
}
//MARK: - Initial
extension StartGameViewModel {
  
  convenience init(viewController: StartGameViewController) {
    self.init()
    self.VC       = viewController
    self.managers = StartGameManagers(viewModel: self)
  }
}
