
import Foundation

class StartGameManagers: VMManagers {
  
  let setup    : SetupStartGame!
  let server   : ServerStartGame!
  let present  : PresentStartGame!
  let logic    : LogicStartGame!
  let animation: AnimationStartGame!
  let router   : RouterStartGame!
  
  init(viewModel: StartGameViewModel) {
    
    self.setup     = SetupStartGame(viewModel: viewModel)
    self.server    = ServerStartGame(viewModel: viewModel)
    self.present   = PresentStartGame(viewModel: viewModel)
    self.logic     = LogicStartGame(viewModel: viewModel)
    self.animation = AnimationStartGame(viewModel: viewModel)
    self.router    = RouterStartGame(viewModel: viewModel)
  }
}

