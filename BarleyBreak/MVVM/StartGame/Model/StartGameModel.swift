
import UIKit

enum StartGameModel {
	
	case loading
	case getData
	case errorHandler(StartGameData?)
	case presentData(StartGameData)
	
	
}

