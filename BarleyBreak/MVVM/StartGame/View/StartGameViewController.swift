import UIKit
import GoogleMobileAds

class StartGameViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: StartGameViewModel!
  
  //MARK: - Public variable
  
  //MARK: - Outlets
  @IBOutlet weak var logoImageView    : UIImageView!
  @IBOutlet weak var balanceLabel     : UILabel!
  @IBOutlet weak var playLogoImageView: UIImageView!
  
  
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
    self.viewModel.viewDidLoad()
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.viewModel.viewDidLayoutSubviews()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.viewModel.viewDidAppear()
  }
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.viewModel.viewWillAppear()
  }
  private func initViewModel(){
    self.viewModel = StartGameViewModel(viewController: self)
  }
}

extension StartGameViewController: GADBannerViewDelegate {
  
  func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
    print("bannerViewDidReceiveAd")
  }
  
  func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
    print("bannerView:didFailToReceiveAdWithError: \(error.localizedDescription)")
  }
  
  func bannerViewDidRecordImpression(_ bannerView: GADBannerView) {
    print("bannerViewDidRecordImpression")
  }
  
  func bannerViewWillPresentScreen(_ bannerView: GADBannerView) {
    print("bannerViewWillPresentScreen")
  }
  
  func bannerViewWillDismissScreen(_ bannerView: GADBannerView) {
    print("bannerViewWillDIsmissScreen")
  }
  
  func bannerViewDidDismissScreen(_ bannerView: GADBannerView) {
    print("bannerViewDidDismissScreen")
  }
}
