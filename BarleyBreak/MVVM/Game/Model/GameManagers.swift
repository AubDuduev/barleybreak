
import Foundation

class GameManagers: VMManagers {
  
  let setup    : SetupGame!
  let server   : ServerGame!
  let present  : PresentGame!
  let logic    : LogicGame!
  let animation: AnimationGame!
  let router   : RouterGame!
  
  init(viewModel: GameViewModel) {
    
    self.setup     = SetupGame(viewModel: viewModel)
    self.server    = ServerGame(viewModel: viewModel)
    self.present   = PresentGame(viewModel: viewModel)
    self.logic     = LogicGame(viewModel: viewModel)
    self.animation = AnimationGame(viewModel: viewModel)
    self.router    = RouterGame(viewModel: viewModel)
  }
}

