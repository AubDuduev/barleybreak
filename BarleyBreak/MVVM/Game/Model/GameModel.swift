
import UIKit

enum GameModel {
	
	case loading
	case getData
	case errorHandler(GameData?)
	case presentData(GameData)
	
	
}

