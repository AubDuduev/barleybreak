import UIKit
import SpriteKit

class GameViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: GameViewModel!
  
  //MARK: - Public variable
  public var gameScene    : SKScene!
  public var gameSceneView: SKView!
  public let endGameView  = EndGameView().loadNib()
  public let noMonetsView = NoMonetsView().loadNib()
  
  
  //MARK: - Outlets
  @IBOutlet weak var gameView    : UIView!
  @IBOutlet weak var balanceLabel: UILabel!
  @IBOutlet weak var lottieView  : UIView!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
    self.viewModel.viewDidLoad()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.viewModel.viewDidAppear()
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.viewModel.viewDidLayoutSubviews()
  }
  private func initViewModel(){
    self.viewModel = GameViewModel(viewController: self)
  }
}
