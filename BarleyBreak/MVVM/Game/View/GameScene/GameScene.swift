
import SpriteKit
import UIKit

class GameScene: SKScene {
  
  //Public
  public var viewModel    : GameViewModel!
  public var level        = 3
  public var searchCompare: SearchCompare!
  public var updateCards  : UpdateCards!
  //public var scoreView    : ScoreView!
  public var cardNodes    = [CardNode]()
  //Private
  private var moveElement    : MoveElement!
  private var gameFon        : GameFonNode!
  private var createCards    : CreateCards!
  private var lightCards     : LightCards!
  private var swipeLogic     : SwipeLogic!
  private var currentScore   = 0
  private var checkForMatches: CheckForMatches!
  
  override func didMove(to view: SKView) {
    super.didMove(to: view)
    self.addGameFon()
    self.createNewGame()
    self.setupMoveElement()
    self.setupSwipeLogic()
    self.setupSearchCompare()
  }
  private func addGameFon(){
    self.gameFon = GameFonNode(color: .clear, size: self.size)
    self.gameFon.setup(scene: self)
    self.addChild(self.gameFon)
  }
  private func setupSwipeLogic(){
    self.swipeLogic = SwipeLogic(gameFon    : self.gameFon,
                                 gameScene  : self,
                                 moveElement: self.moveElement)
    self.swipeLogic.setup()
  }
  private func setupMoveElement(){
    self.moveElement = MoveElement(gameFon: self.gameFon)
  }
  private func setupSearchCompare(){
    self.searchCompare = SearchCompare(gameScene: self, gameFon: self.gameFon)
    //self.searchCompare.searchAllCompare(count: self.level)
  }
  private func setupCheckForMatches(){
    self.checkForMatches = CheckForMatches(gameFon     : self.gameFon,
                                           matchesArray: createCards.matchesArray)
    self.checkForMatches.closure = { success in
      guard success else { return }
      self.lightAllNodes()
      self.viewModel.managers.logic.calculate(type: .plus(1000))
      self.viewModel.managers.logic.endGameViewIsHidden(is: false)
    }
    self.checkForMatches.matches(count: self.level)
    
  }
  public func createNewGame(){
    self.createCards = CreateCards(gameFon: self.gameFon, gameScene: self)
    self.createCards.create(count: self.level)
  }
  public func removeGame(){
    self.updateCards = UpdateCards(gameFon: self.gameFon)
    self.updateCards.removeAll(count: self.level)
  }
  public func moveElements(elementTouch: CardNode){
    if Score.shared.isBalance(sum: 10) {
      self.swipeLogic.cardTouch = elementTouch
      DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
        self.setupCheckForMatches()
        self.viewModel.managers.logic.calculate(type: .minus(10))
      }
    } else {
      self.viewModel.managers.logic.alertNoCoin()
    }
  }
  public func lightAllNodes(){
    self.lightCards = LightCards(gameFon: self.gameFon)
    self.lightCards.all(count: self.level)
  }
  convenience init?(fileNamed: SceneName, viewModel: GameViewModel) {
    self.init(fileNamed: fileNamed.rawValue)
    self.viewModel = viewModel
  }
}
