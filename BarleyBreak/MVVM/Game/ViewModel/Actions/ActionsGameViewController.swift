
import UIKit

extension GameViewController {
  
  @IBAction func startGameVCButton(button: UIButton){
    self.viewModel.managers.logic.startGameVC()
  }
  @IBAction func menuVCButton(button: UIButton){
    self.viewModel.managers.logic.menuVC()
  }
  @IBAction func repeatGameButton(button: UIButton){
    self.viewModel.managers.logic.repeatGame()
  }
  @IBAction func numbersGameButton(button: UIButton){
    self.viewModel.managers.logic.numbersGame(button: button)
  }
}
