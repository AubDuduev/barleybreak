import UIKit

class AnimationGame: VMManager {
  
  //MARK: - Public variable
  public var VM: GameViewModel!
  
  
}
//MARK: - Initial
extension AnimationGame {
  
  //MARK: - Inition
  convenience init(viewModel: GameViewModel) {
    self.init()
    self.VM = viewModel
  }
}

