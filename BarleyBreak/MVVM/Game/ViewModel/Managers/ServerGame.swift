import UIKit

class ServerGame: VMManager {
  
  //MARK: - Public variable
  public var VM: GameViewModel!
  
  
}
//MARK: - Initial
extension ServerGame {
  
  //MARK: - Inition
  convenience init(viewModel: GameViewModel) {
    self.init()
    self.VM = viewModel
  }
}


