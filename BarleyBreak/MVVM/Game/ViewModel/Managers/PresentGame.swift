
import UIKit

class PresentGame: VMManager {
  
  //MARK: - Public variable
  public var VM: GameViewModel!
  
  
}
//MARK: - Initial
extension PresentGame {
  
  //MARK: - Inition
  convenience init(viewModel: GameViewModel) {
    self.init()
    self.VM = viewModel
  }
}

