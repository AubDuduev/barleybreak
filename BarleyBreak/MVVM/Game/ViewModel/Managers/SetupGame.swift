
import UIKit
import SpriteKit
import SnapKit

class SetupGame: VMManager {
  
  //MARK: - Public variable
  public var VM: GameViewModel!
  
  
  public func gameScene(){
    self.VM.VC.gameScene = GameScene(fileNamed: .GameScene, viewModel: self.VM)
    self.VM.VC.gameScene.scaleMode = .resizeFill
    self.VM.VC.gameScene.backgroundColor = #colorLiteral(red: 0.2645639747, green: 0.147419033, blue: 0.0005472909386, alpha: 1)
  }
  public func gameSceneView(){
    self.VM.VC.gameSceneView = SKView()
    self.VM.VC.gameSceneView.backgroundColor     = #colorLiteral(red: 0.2645639747, green: 0.147419033, blue: 0.0005472909386, alpha: 1)
    self.VM.VC.gameSceneView.ignoresSiblingOrder = false
    self.VM.VC.gameSceneView.showsFPS            = true
    self.VM.VC.gameSceneView.showsNodeCount      = true
    self.VM.VC.gameSceneView.contentMode         = .scaleAspectFit
    self.VM.VC.gameSceneView.shadowColor(color: .black, radius: 20)
    self.VM.VC.gameSceneView.cornerRadius(5, true)
  }
  public func didViewScene(){
    guard self.VM.VC.gameScene.view == nil else { return }
    self.VM.VC.gameSceneView.presentScene(self.VM.VC.gameScene)
  }
  public func gameView(){
    self.VM.VC.gameView.alpha = 0
    self.VM.VC.gameView.cornerRadius(5, false)
    self.VM.VC.gameView.shadowColor(color: .black, radius: 20)
    self.VM.VC.gameView.addSubview(self.VM.VC.gameSceneView)
    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
      UIView.animate(withDuration: 0.5) { [weak self] in
        self?.VM.VC.gameView.alpha = 1
      }
    }
  }
  public func gameSceneViewConstraints(){
    self.VM.VC.gameSceneView.frame               = self.VM.VC.gameView.frame
    self.VM.VC.gameSceneView.snp.makeConstraints { (gameSceneView) in
      gameSceneView.edges.equalTo(self.VM.VC.gameView).inset(2)
    }
  }
  public func balanceLabel(){
    Score.shared.setBalance(label: self.VM.VC.balanceLabel)
  }
  public func endGameView(){
    self.VM.VC.view.addSubview(self.VM.VC.endGameView)
    self.VM.VC.endGameView.snp.makeConstraints { (endGameView) in
      endGameView.edges.equalTo(self.VM.VC.view)
    }
    self.VM.managers.logic.endGameViewIsHidden(is: true)
    self.VM.VC.endGameView.continueClosure = { [weak self] success in
      self?.VM.managers.logic.endGameViewIsHidden(is: success)
      self?.VM.managers.logic.repeatGame()
    }
  }
  public func lottieSetup(){
    self.VM.lottieSetup = GDLottieSetup(view: self.VM.VC.lottieView)
    let lightning = GDLottieSetup.LottieName.lightning.rawValue
    self.VM.lottieSetup.added(name: lightning, loopMode: .autoReverse, speed: 1)
    self.VM.lottieSetup.play()
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
      self.VM.VC.lottieView.isHidden = true
    }
  }
}
//MARK: - Initial
extension SetupGame {
  
  //MARK: - Inition
  convenience init(viewModel: GameViewModel) {
    self.init()
    self.VM = viewModel
  }
}


