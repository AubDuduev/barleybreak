import Foundation
import UIKit
import SwiftEntryKit

class LogicGame: VMManager {
  
  //MARK: - Public variable
  public var VM: GameViewModel!
  
  public func startGameVC(){
    self.VM.managers.router.push(.StartGameVC)
  }
  public func repeatGame(){
    (self.VM.VC.gameScene as! GameScene).removeGame()
    (self.VM.VC.gameScene as! GameScene).createNewGame()
  }
  public func menuVC(){
    self.VM.managers.router.push(.MenuVC)
  }
  public func calculate(type: Score.CalculateType){
    if Score.shared.isBalance(sum: 10) {
      Score.shared.calculate(type: type)
      Score.shared.setBalance(label: self.VM.VC.balanceLabel)
    } else {
      self.alertNoCoin()
    }
  }
  public func numbersGame(button: UIButton){
    (self.VM.VC.gameScene as! GameScene).removeGame()
    (self.VM.VC.gameScene as! GameScene).level = button.tag
    (self.VM.VC.gameScene as! GameScene).createNewGame()
  }
  public func endGameViewIsHidden(is hidden: Bool){
    self.VM.VC.endGameView.isHidden = hidden
  }
  public func alertNoCoin(){
    self.VM.VC.noMonetsView.frame = CGRect(x: 0, y: 0, width: 282, height: 333)
    self.VM.VC.noMonetsView.center = self.VM.VC.view.center
    self.VM.VC.noMonetsView.set(type: .noMonet)
    self.VM.VC.noMonetsView.attributesSetup()
    SwiftEntryKit.display(entry: self.VM.VC.noMonetsView, using: self.VM.VC.noMonetsView.attributes)
  }
}
//MARK: - Initial
extension LogicGame {
  
  //MARK: - Inition
  convenience init(viewModel: GameViewModel) {
    self.init()
    self.VM = viewModel
  }
}
