
import UIKit

class RouterGame: VMManager {
  
  //MARK: - Public variable
  public var VM: GameViewModel!
  
  
  public func push(_ type: Push){
    
    switch type {
      case .StartGameVC:
        self.pushStartGameVC()
        
      case .MenuVC:
        self.pushMenu()
    }
  }
  
  enum Push {
    case StartGameVC
    case MenuVC
    
  }
}
//MARK: - Private functions
extension RouterGame {
  
  private func pushStartGameVC(){
    self.VM.VC.dismiss(animated: true, completion: nil)
  }
  private func pushMenu(){
    let menuVC = self.VM.VC.getVCForID(storyboardID     : .Menu,
                                       vcID             : .MenuVC,
                                       transitionStyle  : .crossDissolve,
                                       presentationStyle: .fullScreen) as! MenuViewController
    self.VM.VC.present(menuVC, animated: true)
  }
}
//MARK: - Initial
extension RouterGame {
  
  //MARK: - Inition
  convenience init(viewModel: GameViewModel) {
    self.init()
    self.VM = viewModel
  }
}



