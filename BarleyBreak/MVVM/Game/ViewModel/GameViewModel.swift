
import Foundation

class GameViewModel: VMManagers {
	
	public var gameModel: GameModel = .loading {
		didSet{
			self.logicGameModel()
		}
	}
  
  //MARK: - Public variable
  public var lottieSetup: GDLottieSetup!
  public var managers   : GameManagers!
  public var VC         : GameViewController!
  
  public func viewDidLoad() {
    self.managers.setup.endGameView()
    self.managers.setup.gameScene()
    self.managers.setup.gameSceneView()
    self.managers.setup.gameView()
    self.managers.setup.gameSceneViewConstraints()
    self.managers.setup.lottieSetup()
  }
  public func viewDidAppear() {
    self.managers.setup.didViewScene()
    self.managers.setup.balanceLabel()
  }
  public func viewDidLayoutSubviews() {
    self.managers.setup.gameSceneViewConstraints()
  }
  
  public func logicGameModel(){
    
    switch self.gameModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
}
//MARK: - Initial
extension GameViewModel {
  
  convenience init(viewController: GameViewController) {
    self.init()
    self.VC       = viewController
    self.managers = GameManagers(viewModel: self)
  }
}
