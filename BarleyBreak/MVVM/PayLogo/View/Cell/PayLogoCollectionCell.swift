
import UIKit

class PayLogoCollectionCell: UICollectionViewCell, LoadNidoble {
  
 
  private var viewModel: PayLogoViewModel!
  
  @IBOutlet weak var playLogoImageView: UIImageView!
  @IBOutlet weak var playLogoButton   : UIButton!
  
  public func configure(viewModel: PayLogoViewModel?, indexPath: IndexPath){
    self.viewModel = viewModel
    let logo  = PlayLogoImages.allCases[indexPath.row]
    let price = PlayLogoPrice.allCases[indexPath.row].rawValue
    self.playLogoButton.setTitle("\(price)", for: .normal)
    self.playLogoImageView.image = UIImage(named: logo.rawValue)
  }
  @IBAction func payLogoButton(button: UIButton){
    self.viewModel.managers.logic.payLogo()
  }
}
