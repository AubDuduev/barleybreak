import UIKit

class PayLogoViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModel: PayLogoModelManager!
  
  //MARK: - Public variable
  public let noMonetsView = NoMonetsView().loadNib()
  
  //MARK: - Outlets
  @IBOutlet weak var balanceLabel         : UILabel!
  @IBOutlet weak var payLogoCollectionView: UICollectionView!
  @IBOutlet weak var payLogoCollection    : PayLogoCollection!
  
 
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModel()
    self.viewModel.viewDidLoad?()
  }
  private func initViewModel(){
    self.viewModel = PayLogoViewModel(viewController: self)
  }
}

