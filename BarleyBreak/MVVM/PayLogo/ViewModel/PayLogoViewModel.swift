
import Foundation

class PayLogoViewModel: VMManagers, PayLogoModelProtocol {
	
	public var payLogoModel: PayLogoModel = .loading {
		didSet{
			self.logicPayLogoModel()
		}
	}
  
  //MARK: - Public variable
  public var managers: PayLogoManagers!
  public var VC      : PayLogoViewController!
  
  public func viewDidLoad() {
    self.managers.setup.payLogoCollection()
    self.managers.present.balanceLabel()
    
  }
  
  public func logicPayLogoModel(){
    
    switch self.payLogoModel {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
      case .scrollPage(let buttonTag):
        self.managers.logic.scrollPage(buttonTag: buttonTag)
    }
  }
}
//MARK: - Initial
extension PayLogoViewModel {
  
  convenience init(viewController: PayLogoViewController) {
    self.init()
    self.VC       = viewController
    self.managers = PayLogoManagers(viewModel: self)
  }
}
