
import UIKit

class PayLogoCollection : NSObject {
  
  //MARK: - Variable
  public var collectionView: UICollectionView!
  public var viewModel     : PayLogoViewModel!
  public var logos         = PlayLogoImages.allCases
}

//MARK: - Delegate CollectionView
extension PayLogoCollection: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
  }
  func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    self.viewModel.managers.logic.indexPath = indexPath
  }
}

//MARK: - DataSource CollectionView
extension PayLogoCollection: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    self.collectionView = collectionView
  return self.logos.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = PayLogoCollectionCell().collectionCell(collectionView, indexPath: indexPath)
    cell.configure(viewModel: self.viewModel, indexPath: indexPath)
    return cell
  }
}
//MARK: - DelegateFlowLayout  CollectionView
extension PayLogoCollection: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let width : CGFloat = collectionView.bounds.width
    let height: CGFloat = collectionView.bounds.height 
    return .init(width: width, height: height)
  }
  
}


