import Foundation
import UIKit
import SwiftEntryKit

class LogicPayLogo: VMManager {
  
  //MARK: - Public variable
  public var VM: PayLogoViewModel!
  public var page      = 0
  public var logos     = PlayLogoImages.allCases
  public var indexPath = IndexPath(item: -1, section: 0)
  
  public func scrollPage(buttonTag: Int){
    
    let direction = PageDirection(rawValue: buttonTag)!
    
    switch direction {
      case .Left:
        page -= 1
        guard page > -1 else { page += 1 ; return }
        self.VM.VC.payLogoCollectionView.scrollToItem(at: IndexPath(item: page, section: 0), at: .centeredHorizontally, animated: true)
      case .Right:
        page += 1
        guard logos.count > page else { page = 0 ; return }
        self.VM.VC.payLogoCollectionView.scrollToItem(at: IndexPath(item: page, section: 0), at: .centeredHorizontally, animated: true)
    }
  }
  public func payLogo(){
    self.alertNoCoin()
    self.VM.VC.noMonetsView.callback = { [weak self] in
      guard let self = self else { return }
      let price = PlayLogoPrice.allCases[self.indexPath.row + 1].rawValue
      if Score.shared.isBalance(sum: price) {
        let payLogo = PlayLogoImages.allCases[self.indexPath.row + 1].rawValue
        Score.shared.calculate(type: .minus(price))
        self.VM.managers.present.balanceLabel()
        StoreProject.shared.saveString(key: .payLogo, value: payLogo)
      }
    }
  }
  public func alertNoCoin(){
    self.VM.VC.noMonetsView.frame = CGRect(x: 0, y: 0, width: 282, height: 333)
    self.VM.VC.noMonetsView.center = self.VM.VC.view.center
    self.VM.VC.noMonetsView.set(type: .youPay)
    self.VM.VC.noMonetsView.attributesSetup()
    SwiftEntryKit.display(entry: self.VM.VC.noMonetsView, using: self.VM.VC.noMonetsView.attributes)
  }
}
//MARK: - Initial
extension LogicPayLogo {
  
  //MARK: - Inition
  convenience init(viewModel: PayLogoViewModel) {
    self.init()
    self.VM = viewModel
  }
}

enum PageDirection: Int {
  case Left
  case Right
}
