
import UIKit

class PresentPayLogo: VMManager {
  
  //MARK: - Public variable
  public var VM: PayLogoViewModel!
  
  public func balanceLabel(){
    Score.shared.setBalance(label: self.VM.VC.balanceLabel)
  }
}
//MARK: - Initial
extension PresentPayLogo {
  
  //MARK: - Inition
  convenience init(viewModel: PayLogoViewModel) {
    self.init()
    self.VM = viewModel
  }
}

