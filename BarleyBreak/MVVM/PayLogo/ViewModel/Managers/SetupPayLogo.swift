
import UIKit

class SetupPayLogo: VMManager {
  
  //MARK: - Public variable
  public var VM: PayLogoViewModel!
  
  public func payLogoCollection(){
    let collectionViewLayout = PayLogoCollectionViewLayout()
    collectionViewLayout.sectionInset = .init(top: 10, left: 0, bottom: 10, right: 0)
    collectionViewLayout.sectionInsetReference   = .fromContentInset
    collectionViewLayout.minimumLineSpacing      = 0
    collectionViewLayout.minimumInteritemSpacing = 0
    collectionViewLayout.scrollDirection         = .horizontal
    self.VM.VC.payLogoCollectionView.isPagingEnabled      = true
    self.VM.VC.payLogoCollectionView.collectionViewLayout = collectionViewLayout
    self.VM.VC.payLogoCollection.collectionView           = self.VM.VC.payLogoCollectionView
    self.VM.VC.payLogoCollection.viewModel                = self.VM
  }
}
//MARK: - Initial
extension SetupPayLogo {
  
  //MARK: - Inition
  convenience init(viewModel: PayLogoViewModel) {
    self.init()
    self.VM = viewModel
  }
}


