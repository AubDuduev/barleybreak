import UIKit

class ServerPayLogo: VMManager {
  
  //MARK: - Public variable
  public var VM: PayLogoViewModel!
  
  
}
//MARK: - Initial
extension ServerPayLogo {
  
  //MARK: - Inition
  convenience init(viewModel: PayLogoViewModel) {
    self.init()
    self.VM = viewModel
  }
}


