import UIKit

class AnimationPayLogo: VMManager {
  
  //MARK: - Public variable
  public var VM: PayLogoViewModel!
  
  
}
//MARK: - Initial
extension AnimationPayLogo {
  
  //MARK: - Inition
  convenience init(viewModel: PayLogoViewModel) {
    self.init()
    self.VM = viewModel
  }
}

