//
//  ActionsPayLogoViewController.swift
//  BarleyBreak
//
//  Created by Senior Developer on 04.05.2021.
//
import UIKit

extension PayLogoViewController {
  
  @IBAction func backButton(button: UIButton){
    self.dismiss(animated: true)
  }
  @IBAction func scrollPage(button: UIButton){
    self.viewModel.payLogoModel = .scrollPage(button.tag)
  }
}
