
import UIKit

enum PayLogoModel {
	
	case loading
	case getData
	case errorHandler(PayLogoData?)
	case presentData(PayLogoData)
	case scrollPage(Int)
	
}

enum PlayLogoImages: String, CaseIterable {
  
  case playLogo1
  case playLogo2
  case playLogo3
  case playLogo4
  case playLogo5
  case playLogo6
  case playLogo7
  case playLogo8
  case playLogo9
  case playLogo10
  case playLogo11
  case playLogo12
  case playLogo13
  case playLogo14
  case playLogo15
  case playLogo16
  case playLogo17
  case playLogo18
}

enum PlayLogoPrice: Int, CaseIterable {
  
  case playLogo1  = 100
  case playLogo2  = 150
  case playLogo3  = 200
  case playLogo4  = 250
  case playLogo5  = 300
  case playLogo6  = 350
  case playLogo7  = 400
  case playLogo8  = 450
  case playLogo9  = 500
  case playLogo10 = 550
  case playLogo11 = 600
  case playLogo12 = 650
  case playLogo13 = 700
  case playLogo14 = 750
  case playLogo15 = 800
  case playLogo16 = 850
  case playLogo17 = 900
  case playLogo18 = 1000
}
