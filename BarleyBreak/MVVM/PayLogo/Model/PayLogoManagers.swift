
import Foundation

class PayLogoManagers: VMManagers {
  
  let setup    : SetupPayLogo!
  let server   : ServerPayLogo!
  let present  : PresentPayLogo!
  let logic    : LogicPayLogo!
  let animation: AnimationPayLogo!
  let router   : RouterPayLogo!
  
  init(viewModel: PayLogoViewModel) {
    
    self.setup     = SetupPayLogo(viewModel: viewModel)
    self.server    = ServerPayLogo(viewModel: viewModel)
    self.present   = PresentPayLogo(viewModel: viewModel)
    self.logic     = LogicPayLogo(viewModel: viewModel)
    self.animation = AnimationPayLogo(viewModel: viewModel)
    self.router    = RouterPayLogo(viewModel: viewModel)
  }
}

