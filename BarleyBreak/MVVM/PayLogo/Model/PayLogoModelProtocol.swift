//
//  PayLogoModelProtocol.swift
//  BarleyBreak
//
//  Created by Senior Developer on 05.05.2021.
//

import Foundation

protocol PayLogoModelProtocol {
  
  var payLogoModel: PayLogoModel { get set }
  
}

typealias PayLogoModelManager = PayLogoModelProtocol & VMManagers
