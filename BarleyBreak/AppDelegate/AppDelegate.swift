
import AppsFlyerLib
import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
   
   var window: UIWindow?
   public var orientationLock = UIInterfaceOrientationMask.all
   
   private let rootVC = RootVC()
   private let setupAppsFlyer    = GDAppsFlyerSetup()
   private let setupFirebase     = GDSetupFirebase()
   private let setupFacebook     = GDSetupFacebook()
   private let setupAppMetrica   = GDSetupAppMetrica()
   private let setupNotification = GDSetupNotification()
   private let groupIDFA         = DispatchGroup()
   private let groupAppsFlayer   = DispatchGroup()
   private let addMob            = GDAddMob()
   
   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      self.root()
      //GDSetupNotification
      self.setupNotification.setup(application: application)
      //GDSetupFirebase
      self.setupFirebase.setup()
      self.setupFirebase.setupRemoteConfig()
      //Create URL
      self.createURL()
      //GDAddMob
      self.addMob.setup()
      //GDAppsFlyerSetup
      let workItemIDFA = DispatchWorkItem {
         //setup
         self.setupAppsFlyer.setup()
         print("setupAppsFlyer success")
      }
      //IDFA Делаем запрос на IDFA
      self.groupIDFA.enter()
      IDFA.shared.getRequest { success in
         guard success else { return }
         self.groupIDFA.leave()
         print("groupIDFA success")
      }
      self.groupIDFA.notify(queue: .main, work: workItemIDFA)
      
      
      //GDSetupFacebook
      _ = self.setupFacebook.setup(application, launchOptions)
      //GDSetupAppMetrica
      self.setupAppMetrica.activate()
      
      return true
   }
   
   private func createURL(){
      
      var parameters: String!
      var path      : String!
      
      //1 - Получаем параметры
      self.groupAppsFlayer.enter()
      self.setupAppsFlyer.appsFlyerGetData.getUrlParameters = { getParameters, isCampaign in
         parameters = getParameters
         self.setupFirebase.setupAnalytics(isOrganic: isCampaign)
         print("getParameters \(getParameters) success")
         guard !StoreProject.shared.getBool(key: .isOpenURL) else { return }
         self.groupAppsFlayer.leave()
      }
      
      //2 - Получаем основной URL
      self.groupAppsFlayer.enter()
      self.setupFirebase.getSetting = { setting in
         path = setting.path ?? ""
         
         guard setting.openWB else { return }
         self.groupAppsFlayer.leave()
         print("setupFirebase \(setting) success")
      }
      
      //3 - Показываем рекламу
      self.groupAppsFlayer.notify(queue: .main) {
         let url = path + parameters
         //сохраняем URL для рекламы
         StoreProject.shared.saveString(key: .mainURL, value: url)
         self.rootVC.set(window: self.window, root: .WebView)
      }
   }
   private func root(){
      if StoreProject.shared.getBool(key: .isOpenURL) {
         //RootVC
         self.rootVC.set(window: self.window, root: .WebView)
      } else {
         self.rootVC.set(window: self.window, root: .Loading)
      }
   }
   func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
      // Reports app open from deep link for iOS 10 or later
      AppsFlyerLib.shared().handleOpen(url, options: options)
      return true
   }
   func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
      //AppsFlyer
      AppsFlyerLib.shared().continue(userActivity, restorationHandler: nil)
      return true
   }
   func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool{
      //AppsFlyerLib
      AppsFlyerLib.shared().handleOpen(url, sourceApplication: sourceApplication, withAnnotation: annotation)
      return true
   }
   // Reports app open from a Universal Link for iOS 9 or later
   func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
      AppsFlyerLib.shared().continue(userActivity, restorationHandler: restorationHandler)
      return true
   }
   func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
      
   }
   func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data){
      //appMetrica post deviceToken
      self.setupAppMetrica.setDeviceToken(deviceToken: deviceToken)
      //AppsFlyer notification remove
      AppsFlyerLib.shared().registerUninstall(deviceToken)
   }
   func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
      //setup YandexMetrica
      self.setupAppMetrica.handlePushNotification(userInfo)
      //setup AppsFlyer
      self.setupAppsFlyer.appsFlyerGetData.onConversionDataSuccess(userInfo)
      self.setupAppsFlyer.appsFlyerGetData.onAppOpenAttribution(userInfo)
      AppsFlyerLib.shared().handlePushNotification(userInfo)
      //Print full message.
      print(userInfo, "didReceiveRemoteNotification AppDelegate")
   }
   func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      //setup YandexMetrica
      self.setupAppMetrica.handlePushNotification(userInfo)
      //setup AppsFlyer
      self.setupAppsFlyer.appsFlyerGetData.onConversionDataSuccess(userInfo)
      print(userInfo, "didReceiveRemoteNotification fetchCompletionHandler AppDelegate")
      completionHandler(.newData)
   }
   func applicationDidBecomeActive(_ application: UIApplication) {
      // Start the SDK (start the IDFA timeout set above, for iOS 14 or later)
      AppsFlyerLib.shared().start()
   }
   func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
      return self.orientationLock
   }
}

